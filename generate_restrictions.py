import argparse
import pathlib
import subprocess

from dolfin import *
from multiphenics import *

from meshhelper.readdomains import readdomains

parameters["ghost_mode"] = "shared_facet"


def generate_subdomain_restriction(mesh, subdomains, subdomain_ids):
    D = mesh.topology().dim()
    # Initialize empty restriction
    restriction = MeshRestriction(mesh, None)
    for d in range(D + 1):
        mesh_function_d = MeshFunction("bool", mesh, d)
        mesh_function_d.set_all(False)
        restriction.append(mesh_function_d)
    # Mark restriction mesh functions based on subdomain id
    for c in cells(mesh):
        if isinstance(subdomain_ids, list):
            id_in = subdomains[c] in subdomain_ids
        else:
            id_in = subdomains[c] == subdomain_ids
        if id_in:
            restriction[D][c] = True
            for d in range(D):
                for e in entities(c, d):
                    restriction[d][e] = True
    # Return
    return restriction


# Helper function to generate interface restriction based on a pair of gmsh subdomain ids
def generate_interface_restriction(mesh, subdomains, subdomain_ids):
    if isinstance(subdomain_ids, list):
        for sub in subdomain_ids:
            assert isinstance(sub, set)
            assert len(sub) == 2
    else:
        assert isinstance(subdomain_ids, set)
        assert len(subdomain_ids) == 2
    D = mesh.topology().dim()
    # Initialize empty restriction
    restriction = MeshRestriction(mesh, None)
    for d in range(D + 1):
        mesh_function_d = MeshFunction("bool", mesh, d)
        mesh_function_d.set_all(False)
        restriction.append(mesh_function_d)
    # Mark restriction mesh functions based on subdomain ids (except the mesh function corresponding to dimension D, as it is trivially false)
    for f in facets(mesh):
        subdomains_ids_f = {subdomains[c] for c in cells(f)}
        assert len(subdomains_ids_f) in (1, 2)
        if isinstance(subdomain_ids, list):
            id_in = subdomains_ids_f in subdomain_ids
        else:
            id_in = subdomains_ids_f == subdomain_ids
        if id_in:
            restriction[D - 1][f] = True
            for d in range(D - 1):
                for e in entities(f, d):
                    restriction[d][e] = True
    # Return
    return restriction


def generate_terminal_restriction(mesh, facet_domain, subdomain_ids):
    D = mesh.topology().dim()
    # Initialize empty restriction
    restriction = MeshRestriction(mesh, None)
    for d in range(D + 1):
        mesh_function_d = MeshFunction("bool", mesh, d)
        mesh_function_d.set_all(False)
        restriction.append(mesh_function_d)
    # Mark restriction mesh functions based on subdomain ids (except the mesh function corresponding to dimension D, as it is trivially false)
    for f in facets(mesh):
        facet_id = facet_domain[f]
        if isinstance(subdomain_ids, list):
            id_in = facet_id in subdomain_ids
        else:
            id_in = facet_id == subdomain_ids
        if id_in:
            restriction[D - 1][f] = True
            for d in range(D - 1):
                for e in entities(f, d):
                    restriction[d][e] = True
            for e in entities(f, D):
                restriction[D][e] = True
    # Return
    return restriction


def generate_surface_restriction(
    mesh, subdomains, subdomain_ids, facet_domains, boundary_id, exceptions=None
):
    if isinstance(subdomain_ids, list):
        for sub in subdomain_ids:
            #            assert len(sub) is 2
            assert isinstance(sub, set)
    else:
        assert isinstance(subdomain_ids, set)
        assert len(subdomain_ids) == 2
    D = mesh.topology().dim()
    # Initialize empty restriction
    restriction = MeshRestriction(mesh, None)
    for d in range(D + 1):
        mesh_function_d = MeshFunction("bool", mesh, d)
        mesh_function_d.set_all(False)
        restriction.append(mesh_function_d)
    # Mark restriction mesh functions based on subdomain ids (except the mesh function corresponding to dimension D, as it is trivially false)
    for f in facets(mesh):
        cells_ = [c for c in cells(f)]
        subdomains_ids_f = {subdomains[c] for c in cells_}
        facet_id = facet_domains[f]
        assert len(subdomains_ids_f) in (1, 2)
        if isinstance(subdomain_ids, list):
            ids_to_test = [s for s in subdomain_ids if len(s) == len(cells_)]
            id_in = subdomains_ids_f in ids_to_test
        else:
            id_in = subdomains_ids_f == subdomain_ids
        if exceptions and facet_id in exceptions:
            id_in = False

        if id_in or facet_id in boundary_id:
            restriction[D - 1][f] = True
            for d in range(D - 1):
                for e in entities(f, d):
                    restriction[d][e] = True
    # Return
    return restriction


def generate_interior(mesh, subdomains, subdomain_ids):
    assert isinstance(subdomain_ids, set)
    D = mesh.topology().dim()
    mesh_function_d = MeshFunction("size_t", mesh, D - 1)
    mesh_function_d.set_all(1)
    for f in facets(mesh):
        cells_ = [c for c in cells(f)]
        assert len(cells_) < 3
        if len(cells_) == 2:
            subdomains_ids_f = {subdomains[c] for c in cells_}
            if subdomains_ids_f.issubset(subdomain_ids):
                mesh_function_d[f] = 0
    # Return
    return mesh_function_d


def run(directory, name):

    comm = MPI.comm_world
    domains = readdomains(str(pathlib.PurePath(directory).joinpath("%s.msh" % name)))
    mesh = Mesh(str(pathlib.PurePath(directory).joinpath("%s.xml" % name)))
    cell_domains = MeshFunction(
        "size_t",
        mesh,
        str(pathlib.PurePath(directory).joinpath("%s_physical_region.xml" % name)),
    )
    facet_domain = MeshFunction(
        "size_t",
        mesh,
        str(pathlib.PurePath(directory).joinpath("%s_facet_region.xml" % name)),
    )
    print("Constructing Domains")

    pterm = domains["PTerm"]
    PGrid = domains["PGrid"]
    PAM = domains["PAM"]
    nterm = domains["NTerm"]
    NGrid = domains["NGrid"]
    NAM = domains["NAM"]
    elec = domains["Electrolyte"]
    # case=domains["Case"]
    elec_r = domains["ElectrolyteReference"]
    # separator=domains["Separator"]

    electrode_restriction = generate_subdomain_restriction(
        mesh, cell_domains, [PAM, PGrid, NAM, NGrid]
    )
    positive_electrode_restriction = generate_subdomain_restriction(
        mesh, cell_domains, [PAM, PGrid]
    )
    negative_electrode_restriction = generate_subdomain_restriction(
        mesh, cell_domains, [NAM, NGrid]
    )
    am_restriction = generate_subdomain_restriction(mesh, cell_domains, [PAM, NAM])
    nam_restriction = generate_subdomain_restriction(mesh, cell_domains, [NAM])
    pam_restriction = generate_subdomain_restriction(mesh, cell_domains, [PAM])
    electrolyte_restriction = generate_subdomain_restriction(
        mesh, cell_domains, [elec, PAM, NAM]
    )
    free_electrolyte_restriction = generate_subdomain_restriction(
        mesh, cell_domains, [elec]
    )

    terminal_restriction = generate_terminal_restriction(
        mesh, facet_domain, [pterm, nterm]
    )
    pterminal_restriction = generate_terminal_restriction(mesh, facet_domain, [pterm])

    electrode_shell = generate_surface_restriction(
        mesh,
        cell_domains,
        [
            {elec, PGrid},
            {elec, PAM},
            {PAM},
            {PGrid},
            {elec, NGrid},
            {elec, NAM},
            {NAM},
            {NGrid},
        ],
        facet_domain,
        [None],
        [pterm, nterm],
    )[2]
    positive_shell = generate_surface_restriction(
        mesh,
        cell_domains,
        [{elec, PGrid}, {elec, PAM}, {PAM}, {PGrid}],
        facet_domain,
        [None],
        [pterm],
    )[2]
    negative_shell = generate_surface_restriction(
        mesh,
        cell_domains,
        [{elec, NGrid}, {elec, NAM}, {NAM}, {NGrid}],
        facet_domain,
        [None],
        [nterm],
    )[2]
    electrolyte_shell = generate_surface_restriction(
        mesh,
        cell_domains,
        [
            {elec, PGrid},
            {elec, NGrid},
            {PAM, PGrid},
            {NAM, NGrid},
            {NAM},
            {PAM},
            {elec},
        ],
        facet_domain,
        [elec_r],
    )[2]

    freeelectrolyte_shell = generate_surface_restriction(
        mesh,
        cell_domains,
        [{elec, PGrid}, {elec, NGrid}, {elec, PAM}, {elec, NAM}, {elec}],
        facet_domain,
        [],
        [],
    )[2]

    XDMFFile(
        str(pathlib.PurePath(directory).joinpath("electrode_restriction.rtc.xdmf"))
    ).write(electrode_restriction)
    XDMFFile(
        str(
            pathlib.PurePath(directory).joinpath(
                "positive_electrode_restriction.rtc.xdmf"
            )
        )
    ).write(positive_electrode_restriction)
    XDMFFile(
        str(
            pathlib.PurePath(directory).joinpath(
                "negative_electrode_restriction.rtc.xdmf"
            )
        )
    ).write(negative_electrode_restriction)
    XDMFFile(
        str(pathlib.PurePath(directory).joinpath("electrolyte_restriction.rtc.xdmf"))
    ).write(electrolyte_restriction)
    XDMFFile(
        str(
            pathlib.PurePath(directory).joinpath(
                "free_electrolyte_restriction.rtc.xdmf"
            )
        )
    ).write(free_electrolyte_restriction)
    XDMFFile(
        str(pathlib.PurePath(directory).joinpath("activemass_restriction.rtc.xdmf"))
    ).write(am_restriction)
    XDMFFile(
        str(pathlib.PurePath(directory).joinpath("nam_restriction.rtc.xdmf"))
    ).write(nam_restriction)
    XDMFFile(
        str(pathlib.PurePath(directory).joinpath("pam_restriction.rtc.xdmf"))
    ).write(pam_restriction)
    XDMFFile(
        str(pathlib.PurePath(directory).joinpath("terminal_restriction.rtc.xdmf"))
    ).write(terminal_restriction)
    XDMFFile(
        str(
            pathlib.PurePath(directory).joinpath(
                "positive_terminal_restriction.rtc.xdmf"
            )
        )
    ).write(pterminal_restriction)
    XDMFFile(str(pathlib.PurePath(directory).joinpath("electrode_shell.xdmf"))).write(
        electrode_shell
    )
    XDMFFile(str(pathlib.PurePath(directory).joinpath("positive_shell.xdmf"))).write(
        positive_shell
    )
    XDMFFile(str(pathlib.PurePath(directory).joinpath("negative_shell.xdmf"))).write(
        negative_shell
    )
    XDMFFile(str(pathlib.PurePath(directory).joinpath("electrolyte_shell.xdmf"))).write(
        electrolyte_shell
    )
    XDMFFile(
        str(pathlib.PurePath(directory).joinpath("freeelectrolyte_shell.xdmf"))
    ).write(freeelectrolyte_shell)

    XDMFFile(comm, str(pathlib.PurePath(directory).joinpath("%s.xdmf" % name))).write(
        mesh
    )
    XDMFFile(
        comm,
        str(pathlib.PurePath(directory).joinpath("%s_physical_region.xdmf" % name)),
    ).write(cell_domains)
    XDMFFile(
        comm, str(pathlib.PurePath(directory).joinpath("%s_facet_region.xdmf" % name))
    ).write(facet_domain)


def rungmsh(directory, name):
    subprocess.call(
        ["gmsh", "-3", str(pathlib.PurePath(directory).joinpath("%s.med" % name))]
    )


def rundolfinconvert(directory, name):
    subprocess.call(
        [
            "dolfin-convert",
            str(pathlib.PurePath(directory).joinpath("%s.msh" % name)),
            str(pathlib.PurePath(directory).joinpath("%s.xml" % name)),
        ]
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate Domainsfor given Mesh")
    parser.add_argument("directory", help="the directory of the mesh")
    parser.add_argument("name", default="Cell", help="the name of the mesh")
    args = parser.parse_args()
    rungmsh(args.directory, args.name)
    rundolfinconvert(args.directory, args.name)
    run(args.directory, args.name)
