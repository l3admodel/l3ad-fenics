def readdomains(filename):
    with open(filename) as f:
        line = f.readline()
        line = line.strip()
        while line != "$PhysicalNames":
            line = f.readline()
            line = line.strip()
        line = f.readline()
        info = []
        domains = dict()
        while True:
            line = f.readline()
            line = line.strip()
            if line == "$EndPhysicalNames":
                break
            (_, d, name) = line.split()
            name = name.strip('"')
            domains.update({name: int(d)})
        return domains
