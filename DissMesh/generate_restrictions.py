from dolfin import *
from multiphenics import *
from readdomains import readdomains

domains = readdomains("Cell.msh")
mesh = Mesh("Cell.xml")
cell_domains = MeshFunction("size_t", mesh, "Cell_physical_region.xml")
facet_domain = MeshFunction("size_t", mesh, "Cell_facet_region.xml")


def generate_subdomain_restriction(mesh, subdomains, subdomain_ids):
    D = mesh.topology().dim()
    # Initialize empty restriction
    restriction = MeshRestriction(mesh, None)
    for d in range(D + 1):
        mesh_function_d = MeshFunction("bool", mesh, d)
        mesh_function_d.set_all(False)
        restriction.append(mesh_function_d)
    # Mark restriction mesh functions based on subdomain id
    for c in cells(mesh):
        if isinstance(subdomain_ids, list):
            id_in = subdomains[c] in subdomain_ids
        else:
            id_in = subdomains[c] == subdomain_ids
        if id_in:
            restriction[D][c] = True
            for d in range(D):
                for e in entities(c, d):
                    restriction[d][e] = True
    # Return
    return restriction


# Helper function to generate interface restriction based on a pair of gmsh subdomain ids
def generate_interface_restriction(mesh, subdomains, subdomain_ids):
    if isinstance(subdomain_ids, list):
        for sub in subdomain_ids:
            assert isinstance(sub, set)
            assert len(sub) == 2
    else:
        assert isinstance(subdomain_ids, set)
        assert len(subdomain_ids) == 2
    D = mesh.topology().dim()
    # Initialize empty restriction
    restriction = MeshRestriction(mesh, None)
    for d in range(D + 1):
        mesh_function_d = MeshFunction("bool", mesh, d)
        mesh_function_d.set_all(False)
        restriction.append(mesh_function_d)
    # Mark restriction mesh functions based on subdomain ids (except the mesh function corresponding to dimension D, as it is trivially false)
    for f in facets(mesh):
        subdomains_ids_f = {subdomains[c] for c in cells(f)}
        assert len(subdomains_ids_f) in (1, 2)
        if isinstance(subdomain_ids, list):
            id_in = subdomains_ids_f in subdomain_ids
        else:
            id_in = subdomains_ids_f == subdomain_ids
        if id_in:
            restriction[D - 1][f] = True
            for d in range(D - 1):
                for e in entities(f, d):
                    restriction[d][e] = True
    # Return
    return restriction


def generate_boundary_restriction(mesh, facet_domains, subdomain_ids):
    D = mesh.topology().dim()
    # Initialize empty restriction
    restriction = MeshRestriction(mesh, None)
    for d in range(D + 1):
        mesh_function_d = MeshFunction("bool", mesh, d)
        mesh_function_d.set_all(False)
        restriction.append(mesh_function_d)
    # Mark restriction mesh functions based on subdomain ids (except the mesh function corresponding to dimension D, as it is trivially false)
    for f in facets(mesh):
        facet_id = facet_domain[f]
        if isinstance(subdomain_ids, list):
            id_in = facet_id in subdomain_ids
        else:
            id_in = facet_id == subdomain_ids
        if id_in:
            restriction[D - 1][f] = True
            for d in range(D - 1):
                for e in entities(f, d):
                    restriction[d][e] = True
    # Return
    return restriction


def generate_surface_restriction(
    mesh, subdomains, subdomain_ids, facet_domains, boundary_id, exceptions=None
):
    if isinstance(subdomain_ids, list):
        for sub in subdomain_ids:
            #            assert len(sub) is 2
            assert isinstance(sub, set)
    else:
        assert isinstance(subdomain_ids, set)
        assert len(subdomain_ids) == 2
    D = mesh.topology().dim()
    # Initialize empty restriction
    restriction = MeshRestriction(mesh, None)
    for d in range(D + 1):
        mesh_function_d = MeshFunction("bool", mesh, d)
        mesh_function_d.set_all(False)
        restriction.append(mesh_function_d)
    # Mark restriction mesh functions based on subdomain ids (except the mesh function corresponding to dimension D, as it is trivially false)
    for f in facets(mesh):
        cells_ = [c for c in cells(f)]
        subdomains_ids_f = {subdomains[c] for c in cells_}
        facet_id = facet_domains[f]
        assert len(subdomains_ids_f) in (1, 2)
        if isinstance(subdomain_ids, list):
            ids_to_test = [s for s in subdomain_ids if len(s) == len(cells_)]
            id_in = subdomains_ids_f in ids_to_test
        else:
            id_in = subdomains_ids_f == subdomain_ids
        if exceptions and facet_id in exceptions:
            id_in = False

        if id_in or facet_id in boundary_id:
            restriction[D - 1][f] = True
            for d in range(D - 1):
                for e in entities(f, d):
                    restriction[d][e] = True
    # Return
    return restriction


print("Constructing Domains")

pterm = domains["PTerm"]
PGrid = domains["PGrid"]
PAM = domains["PAM"]
nterm = domains["NTerm"]
NGrid = domains["NGrid"]
NAM = domains["NAM"]
elec = domains["Electrolyte"]
case = domains["Case"]
elec_s = domains["ElectrolyteSurface"]
elec_r = domains["ElectrolyteReference"]

positive_electrode_restriction = generate_subdomain_restriction(
    mesh, cell_domains, [PAM, PGrid]
)
negative_electrode_restriction = generate_subdomain_restriction(
    mesh, cell_domains, [NAM, NGrid]
)
am_restriction = generate_subdomain_restriction(mesh, cell_domains, [PAM, NAM])
electrolyte_restriction = generate_subdomain_restriction(
    mesh, cell_domains, [elec, PAM, NAM]
)
free_electrolyte_restriction = generate_subdomain_restriction(
    mesh, cell_domains, [elec]
)

terminal_restriction = generate_boundary_restriction(mesh, facet_domain, [pterm])
noslip_restriction = generate_surface_restriction(
    mesh,
    cell_domains,
    [{elec, PGrid}, {elec, NGrid}, {PAM, elec}, {NAM, elec}],
    facet_domain,
    [case],
)[2]

positive_shell = generate_surface_restriction(
    mesh,
    cell_domains,
    [{elec, PGrid}, {elec, PAM}, {PAM}, {PGrid}],
    facet_domain,
    [None],
    [pterm],
)[2]
negative_shell = generate_surface_restriction(
    mesh,
    cell_domains,
    [{elec, NGrid}, {elec, NAM}, {NAM}, {NGrid}],
    facet_domain,
    [None],
    [nterm],
)[2]
electrolyte_shell = generate_surface_restriction(
    mesh,
    cell_domains,
    [{elec, PGrid}, {elec, NGrid}, {PAM, PGrid}, {NAM, NGrid}, {NAM}, {PAM}],
    facet_domain,
    [case, elec_s, elec_r],
)[2]

XDMFFile("positive_electrode_restriction.rtc.xdmf").write(
    positive_electrode_restriction
)
XDMFFile("negative_electrode_restriction.rtc.xdmf").write(
    negative_electrode_restriction
)
XDMFFile("electrolyte_restriction.rtc.xdmf").write(electrolyte_restriction)
XDMFFile("free_electrolyte_restriction.rtc.xdmf").write(free_electrolyte_restriction)
XDMFFile("activemass_restriction.rtc.xdmf").write(am_restriction)
XDMFFile("terminal_restriction.rtc.xdmf").write(terminal_restriction)
XDMFFile("noslip_restriction.xdmf").write(noslip_restriction)
XDMFFile("positive_shell.xdmf").write(positive_shell)
XDMFFile("negative_shell.xdmf").write(negative_shell)
XDMFFile("electrolyte_shell.xdmf").write(electrolyte_shell)

comm = mesh.mpi_comm()
XDMFFile("Cell.xdmf").write(mesh)
XDMFFile("Cell_physical_region.xdmf").write(cell_domains)
XDMFFile("Cell_facet_region.xdmf").write(facet_domain)
XDMFFile("positive_shell.xdmf").write(positive_shell)
XDMFFile("negative_shell.xdmf").write(negative_shell)
XDMFFile("electrolyte_shell.xdmf").write(electrolyte_shell)
XDMFFile("noslip_restriction.xdmf").write(noslip_restriction)
