import os
import sys

import petsc4py

petsc4py.init(sys.argv)
import shutil
import pathlib
import dolfin

dolfin.parameters["form_compiler"]["optimize"] = True
dolfin.parameters["form_compiler"]["cpp_optimize_flags"] = "-O2"

import numpy as np
import multiphenics as mp
from helper.parameter import parameter
import helper.petscevent as event
from itertools import product
from simdata.createTestSystem import createSystem
from simdata.runTestEIS import runtesteis
import multiprocessing
from datetime import datetime
import calendar

mpi_comm = dolfin.MPI.comm_world

Nref = parameter["sulfate"]["Nref"]
tolerance = {
    "macro": {
        "u": {"relative": 1e-3, "absolute": 1e-6},
        "Jct": {"relative": 1, "absolute": np.inf},
        "pbion": {"relative": 1e-4, "absolute": 1e-9},
        "rmean": {"relative": 1e-3, "absolute": 1e-10},
        "N": {"relative": 1e-3, "absolute": 1e10 / parameter["sulfate"]["Nref"]},
        "cacid": {"relative": 1e-4, "absolute": 1e-2},
        "X": {"relative": 1e-2, "absolute": 1e-3},
        "iterm": {"relative": 1, "absolute": np.inf},
        "theta": {"relative": 1e-2, "absolute": 1e-3},
    }
}
mpi_comm.barrier()
# C = np.linspace(0.1, 0.5, 3)
C = [0.18]
K1 = np.linspace(0.1, 1, 3)
K2 = np.linspace(1, 5, 3)
K3 = np.linspace(0.8, 1.2, 3)
S = np.linspace(0.5, 1.5, 3)
N = [5e4, 1e5]

initstates = {
    "cacid": {"c0": 3800.0},
    "X": {"+": {"X0": 0.85, "Xy": 0.1}, "-": {"X0": 0.83, "Xy": 0.1}},
    "N": {"+": {"N0": 1e5}, "-": {"N0": 1e5}},
}

output_dir = "../outputBlock"
mesh_dir = "../SmallCell"
mesh_name = "Cell"
macsys = createSystem(mesh_dir, mesh_name, parameter, tolerance, initstates, 2.0 / 36.0)
p1 = multiprocessing.Process(target=runtesteis, args=(macsys, output_dir))

output_dir = "../outputCell"
mesh_dir = "../AddESunMesh"
mesh_name = "Cell"
macsys = createSystem(mesh_dir, mesh_name, parameter, tolerance, initstates, 2.0)
p2 = multiprocessing.Process(target=runtesteis, args=(macsys, output_dir))
p1.start()
p2.start()
p1.join()
p2.join()
