import dolfin
import electrolyte.sulfuricacid as sa
import numpy as np
from helper.masstransport import MassTransport


class AcidTransport(MassTransport):
    def __init__(self, meshinformation, statespace, PAM, NAM, porosity, T, parameter):
        super().__init__(
            meshinformation=meshinformation,
            statespace=statespace,
            PAM=PAM,
            NAM=NAM,
            porosity=porosity,
            T=T,
            shell=meshinformation.electrolyteshell,
            statename="ca",
            alpha=parameter["electrolyte"]["alpha"],
        )

        c = statespace.u[statespace.namedict["ca"]]
        self.Dacid = sa.SulfuricAcidDiffusionCoefficient(
            c.function_space(), T, statespace.namedict, parameter
        )
        self.cascale = dolfin.Constant(parameter["scale"]["cacid"])
        self.factor = dolfin.Constant(parameter["electrolyte"]["dfactor"])

    @property
    def D(self):
        return self.factor * self.Dacid.value * self.porosity

    def update(self, u):
        self.Dacid.update(u)

    def _S(self, u, udot, v):
        Jp = self.PAM.getJ(u, "ca")
        Jn = self.NAM.getJ(u, "ca")
        scale = self.cascale
        g = super()._S(u, udot, v)
        # g += -dolfin.inv(scale) * (1. + 2 * sa.t_m) * Jp * v * self.PAM.dx
        # g += -dolfin.inv(scale) * (1 - 2 * sa.t_m) * Jn * v * self.NAM.dx
        g += -dolfin.inv(scale) * Jp * v * self.PAM.dx
        g += -dolfin.inv(scale) * Jn * v * self.NAM.dx
        return g
