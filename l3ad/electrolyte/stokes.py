import os

import dolfin
import electrolyte.sulfuricacid as sa
import helper.constants as con
import multiphenics as mp
import numpy as np
import ufl
from dolfin import div
from dolfin import dot
from dolfin import grad
from dolfin import inner


class Stokes:
    def __init__(self, meshinformation, statespace, c, T):
        self.comm = dolfin.MPI.comm_world
        self.comm.barrier()
        #    if self.comm.rank == 0:
        #      print("Creating Stokes")
        self.dx = meshinformation.dx_e
        self.c = c
        self.T = T
        self.meshinformation = meshinformation

        self.volume = dolfin.assemble(1.0 * self.dx)
        self.statespace = statespace
        g = -con.g
        self.g = dolfin.Constant((0, g, 0))
        #    print("g: %e" % float(g))
        self.u = statespace.u
        self.c_ref = dolfin.Constant(0)

        self.beta = dolfin.Constant(35e-6)
        #    print("beta: %e" % float(self.beta))
        self.mu_ref = dolfin.Constant(1)
        self.rho_ref = dolfin.Constant(1)
        self.volume = dolfin.assemble(1.0 * self.dx)
        self.comm.barrier()

    def update(self):
        comm = self.comm
        c_ref = dolfin.assemble(self.c * self.dx) / self.volume
        c_ref = comm.bcast(c_ref, root=0)
        self.c_ref.assign(c_ref)
        _mu = sa.calculateViscosity(c_ref, self.T)
        self.mu_ref.assign(_mu)

    #  rho = sa.calculateMassDensity(c_ref, self.T)
    #  self.rho_ref.assign(rho)

    @property
    def velocity(self):
        return self.statespace.u[0]

    def initialise(self):
        self.update()

    def _A(self, up, vq):
        u, p = mp.block_split(up)
        v, q = mp.block_split(vq)

        def epsilon(_u):
            return dolfin.sym(dolfin.grad(_u))

        def a(_u, _v):
            return 2.0 * dolfin.inner(epsilon(_u), epsilon(_v))

        def b(_u, _v):
            return _u * dolfin.div(_v)

        mu = self.mu_ref
        dx = self.dx
        return [[mu * a(u, v) * dx, -b(p, v) * dx], [b(q, u) * dx, 0]]

    def _b(self, vq):
        dx = self.dx
        v, q = mp.block_split(vq)

        def g(_v):
            return self.beta * (self.c - self.c_ref) * dolfin.dot(self.g, _v)

        return [g(v) * dx, 0]

    def _bc(self, V):
        shell = self.meshinformation.freeelectrolyteshell
        bc1 = mp.DirichletBC(
            V.sub(0), dolfin.Constant((0.0, 0.0, 0.0)), shell, 1, "geometric"
        )
        return [bc1]
