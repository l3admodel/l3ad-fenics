import dolfin
from electrolyte.iontransport import IonTransport
from electrolyte.masstransport import AcidTransport
from electrolyte.stokes import Stokes


class Electrolyte:
    def __init__(
        self, id_dict, PAM, NAM, statespace, meshinformation, porosity, T, parameter
    ):

        self.comm = dolfin.MPI.comm_world
        self.comm.barrier()
        self.id_dict = id_dict

        dx = meshinformation.dx
        self.dx_e = dx(id_dict["electrolyte"])
        # self.dx_s = dx(id_dict["separator"])
        self.dx_pam = dx(id_dict["positive active mass"])
        self.dx_nam = dx(id_dict["negative active mass"])
        self.dx_D = self.dx_pam + self.dx_nam + self.dx_e  # +self.dx_s
        self.ds = meshinformation.ds
        self.meshinformation = meshinformation
        self.u = statespace.u[statespace.namedict["ue"]]
        c = statespace.u[statespace.namedict["ca"]]
        self._c = c

        self.A_surface = dolfin.assemble(
            1.0 * self.ds(self.id_dict["electrolyte reference"])
        )
        self.u_form = dolfin.Form(
            self.u * self.ds(self.id_dict["electrolyte reference"])
        )

        self.porosity = porosity

        self._iontransport = IonTransport(
            meshinformation,
            statespace=statespace,
            PAM=PAM,
            NAM=NAM,
            porosity=porosity,
            c=statespace.u[statespace.namedict["ca"]],
            T=T,
            dx_D=self.dx_D,
            parameter=parameter,
            id_dict=id_dict,
        )

        self._masstransport = AcidTransport(
            meshinformation=meshinformation,
            statespace=statespace,
            PAM=PAM,
            NAM=NAM,
            porosity=porosity,
            T=T,
            parameter=parameter,
        )

        self.comm.barrier()

    @property
    def c(self):
        return self._c

    @property
    def iontransport(self):
        return self._iontransport

    @property
    def masstransport(self):
        return self._masstransport

    def getReferencePotential(self):
        return dolfin.assemble(self.u_form) / self.A_surface

    def setConcentration(self, c0):
        self.masstransport.setConcentration(c0)
