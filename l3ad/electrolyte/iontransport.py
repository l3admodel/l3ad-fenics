import dolfin
import electrolyte.sulfuricacid as sa
from helper.domainvalue import DomainAssigner
from helper.transport import Transport


class IonTransport(Transport):
    def __init__(
        self,
        meshinformation,
        statespace,
        PAM,
        NAM,
        porosity,
        c,
        T,
        dx_D,
        parameter,
        id_dict,
    ):

        super().__init__(
            meshinformation,
            statespace,
            dx_D,
            None,
            meshinformation.electrolyteshell,
            "ue",
            alpha=parameter["electrolyte"]["alpha"],
        )
        self.id = id_dict["electrolyte"]
        self.porosity = porosity
        self.PAM = PAM
        self.NAM = NAM
        self.kh2so4 = sa.SulfuricAcidConductivity(
            c.function_space(), T, statespace.namedict, parameter
        )
        self.factor = dolfin.Constant(parameter["electrolyte"]["factor"])

    def initu(self, u):
        nd = self.statespace.namedict
        ue = u[nd["ue"]]
        V = ue.function_space()
        tmp = dolfin.project(-self.NAM.ueq, V)

        ids = [self.PAM.Id, self.NAM.Id, self.id]
        DomainAssigner(
            self.meshinformation.celldomain, V, self.meshinformation.mesh, ids
        ).assign(ue, tmp)

    @property
    def phi(self):
        # return dolfin.Constant(4.55e6)
        return self.kappa

    @property
    def kappa(self):
        return self.kh2so4.value * self.factor * self.porosity

    def update(self, u):
        self.kh2so4.update(u)

    def _BC(self, u, v):
        return 0

    def _S(self, u, udot, v):
        jp = self.PAM.jElectrolyteInterface(u, udot)
        jn = self.NAM.jElectrolyteInterface(u, udot)
        g = jp * v * self.PAM.dx
        g += jn * v * self.NAM.dx
        return g
