import dolfin
import helper.constants as con
import numpy as np
from helper.coefficient import Coefficient

t_p = 0.72
t_m = 0.28


def calculateMassDensity(C, T):
    a = 1147
    b = 0.06962
    c = -8.09e-7
    d = 4.983e-4
    _C = np.clip(C, 200, 5500)
    return np.clip((a + b * _C + c * _C * _C) / (1.0 + d * T), 1e3, 3e3)


def calculateMolality(C, T):
    _C = np.clip(C, 200, 5500)
    molar_mass = 98.079e-3
    rho = calculateMassDensity(_C, T)
    return np.divide(_C, (rho - _C * molar_mass))


class SulfuricAcidDiffusionCoefficient(Coefficient):
    def __init__(self, W, T, namedict, parameter):
        super().__init__("Dh2so4", W, namedict)
        self.cascale = parameter["scale"]["cacid"]
        self.T = T

    def update(self, u):
        nd = self.namedict
        ca = u[nd["ca"]]
        self.k.vector().set_local(
            calculateDiffusionCoeccient(self.cascale * ca.vector().get_local(), self.T)
        )
        self.k.vector().apply("insert")


def calculateDiffusionCoeccient(C, T):
    #    c = np.clip(C, 2e1, None) * 1e-3
    #    sqrt_c = np.sqrt(c)
    #    exp1 = np.exp(-12.0 * sqrt_c)
    #    C0 = np.exp(7.699 + 0.23521 * (1.0-exp1) - 0.2977*c + 0.04164*c*sqrt_c +
    #                0.02023*c*c)
    #    C1 = (-10.56 - 0.3666 * (1.0-exp1) - 0.1107*c + 0.2888*c*sqrt_c -
    #                0.09151*c*c)
    #    return (np.exp(C0 * (1.0/T - 1.0/298.15) + C1) * 1e-4)
    c = np.clip(C, 100, 5500)
    Ea = 1834
    p0 = -13.5
    p1 = 0.001477
    p1s = -0.04929
    p2s = -1.129e-05
    S = p0 + p1 * c + p1s * c ** (1 / 2) + p2s * c ** (3 / 2) - Ea / T
    return np.exp(S)


def calculateViscosity(C, T):
    # dynamice viscosity
    M = calculateMolality(C, T)
    E = 3000.0
    p0 = 0.0005628
    p1 = 1.767e-8
    p2 = 7.277e-9
    return p0 + (p1 + p2 * M) * np.exp(E / T)


class SulfuricAcidConductivity(Coefficient):
    def __init__(self, W, T, namedict, parameter):
        super().__init__("Kappah2so4", W, namedict)
        self.cascale = parameter["scale"]["cacid"]
        self.T = T

    def update(self, u):
        nd = self.namedict
        c = u[nd["ca"]]
        self.k.vector().set_local(
            calculareIonicConductivity(self.cascale * c.vector().get_local(), self.T)
        )
        self.k.vector().apply("insert")


def calculareIonicConductivity(C, T):
    #    m = calculateMolality(C, T)
    #    a0 = -16.93
    #    a1 = -3.569
    #    a2 = -0.0507
    #    a4 = 0.3394
    #    a5 = 0.3922
    #
    #    b1 = -1.227
    #    b2 = -0.01833
    #    b3 = 5.847
    #    b5 = 0.143
    #
    #    exp1 = np.exp(a4 * np.sqrt(m))
    #    m32 = np.power(m, 1.5)
    #    prefactor = (a0 + a1*m + a2*m*m - a0*exp1 + a5*m32)
    #    expo = -np.exp(b1*m + b2*m*m + b3*exp1 + b5*m32) * (1./T - 1./298.5)
    #    cond = np.exp(prefactor + expo)
    #    return np.maximum(cond, 1.0e-3)
    m = calculateMolality(C, T)
    Ea = 1202
    # 1834;
    p0 = 4.431
    # 7.278;
    p1 = -2.382
    # -1.273;
    p1s = 5.191
    # 3.582;
    p2 = -0.0309
    # -0.007209;
    p2s = 0.4527
    # 0.1633;
    S = p0 + p1 * m + p1s * m ** (1 / 2) + p2s * m ** (3 / 2) + p2 * m ** 2 - Ea / T
    return np.exp(S)
