import json
import os
import shutil
import sys

import dolfin
import multiphenics as mp

dolfin.parameters["ghost_mode"] = "shared_facet"
dolfin.parameters["form_compiler"]["optimize"] = True
dolfin.parameters["form_compiler"]["cpp_optimize_flags"] = "-O2"
dolfin.parameters["linear_algebra_backend"] = "PETSc"

import petsc4py

petsc4py.init(sys.argv)

import numpy as np
from simdata.createTestSystem import createSystem
from simdata.runinit import runinit
from simdata.runpeukert import run
import multiprocessing
from itertools import product, chain
import helper.dictupdate as du

import helper.constants as con

mpi_comm = dolfin.MPI.comm_world
with open("tolerance.json") as fp:
    tolerance = json.load(fp)
with open("bounds.json") as fp:
    bounds = json.load(fp)
mpi_comm.barrier()

initstates = {
    "cacid": {
        "u0": 4.5,
        #        'uy': -0.4
    },
    "X": {
        "+": {
            "u0": 0.99,
        },
        "-": {
            "u0": 0.99,
            #            'uy': 0.15
        },
    },
    "r": {
        "+": {
            "u0": 400,
        },
        "-": {
            #'u0': 300,
            "u0": 400,
            #            'uy': -300,
        },
    },
}
full = True
if full:
    cap = 1.0
    mesh_dir = "../DissTestCell"
    mesh_name = "Cell1Ah"
else:
    cap = 2.0 * 0.5 / 36.0
    mesh_dir = "../SmallCellh"
    mesh_name = "CellCoarse"

h = 3600.0
d = h * 24.0

parameterfolder = "parameter1Ah"


def runC(pars):
    parfile, i = pars
    with open("parameter.json") as json_file:
        parameter = json.load(json_file)
    with open(f"{parameterfolder}/parameter_{parfile}.json") as json_file:
        parameterup = json.load(json_file)
    parameter = du.update(parameter, parameterup)
    istate = initstates.copy()
    rt = parameter["Pb"]["type"]
    output_dir = f"../output/{rt}/peukert/{parfile}/I{i}"
    macsys = createSystem(
        mesh_dir, mesh_name, parameter, tolerance, bounds, istate, cap
    )

    Idch = cap / i
    if i == 2:
        uend = 1.65
    elif i == 5:
        uend = 1.7
    elif i == 10:
        uend = 1.725
    else:
        uend = 1.75
    run(macsys, output_dir, U=uend, I=-Idch, outputstatedt=3600)
    with open("%s/initstates.json" % output_dir, "w") as fp:
        json.dump(istate, fp, indent=4)


files = [
    "2S1A_1",
    "2S1A_2",  #'2S1A_3','2S1A_4',
    "2S2A_2",
    "2S2A_3",  #'2S2A_4','2S2A_5','2S2A_1',
    "ha_1",
    "ha_4",  #'ha_2','ha_3','ha_5',
]
idch = [2, 5, 10, 20]
idch = [20]
pool = multiprocessing.Pool(16)
pool.map(runC, product(files, idch))
