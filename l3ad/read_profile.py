import pstats

p = pstats.Stats("profile.log")
p.strip_dirs().sort_stats("cumtime").print_stats(15)
