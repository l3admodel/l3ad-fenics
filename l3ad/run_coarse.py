import json
import os
import shutil
import sys

import dolfin
import multiphenics as mp

# dolfin.set_log_level(10)

dolfin.parameters["ghost_mode"] = "shared_facet"
dolfin.parameters["form_compiler"]["optimize"] = True
dolfin.parameters["form_compiler"]["cpp_optimize_flags"] = "-O2"
dolfin.parameters["linear_algebra_backend"] = "PETSc"

import petsc4py

petsc4py.init(sys.argv)

import numpy as np
from simdata.createTestSystem import createSystem
from simdata.runinit import runinit
from simdata.runnucvar import run
import multiprocessing
from itertools import product, chain
import helper.dictupdate as du

import helper.constants as con

mpi_comm = dolfin.MPI.comm_world
with open("tolerance.json") as fp:
    tolerance = json.load(fp)
with open("bounds.json") as fp:
    bounds = json.load(fp)
mpi_comm.barrier()

initstates = {
    "cacid": {
        "u0": 4.5,
    },
    "X": {
        "+": {
            "u0": 1,
        },
        "-": {
            "u0": 1,
        },
    },
    "n": {"+": {"u0": 0}, "-": {"u0": 0}},
}
full = False
if full:
    cap = 1.0
    mesh_dir = "../DissTestCell"
    mesh_name = "Cell1Ah"
else:
    cap = 2.0 * 0.5 / 36.0
    mesh_dir = "../SmallCellh"
    mesh_name = "CellCoarse"

h = 3600.0
d = h * 24.0

parameterfolder = "parameter1Ah"
Qdch = 0.1
Icha = 1.0 / 20
tcha = Qdch / Icha * h
tpau = 14 * d


def runC(pars):
    parfile, i = pars
    with open("parameter.json") as json_file:
        parameter = json.load(json_file)
    with open(f"{parameterfolder}/parameter_{parfile}.json") as json_file:
        parameterup = json.load(json_file)
    parameter = du.update(parameter, parameterup)
    istate = initstates.copy()
    rt = parameter["Pb"]["type"]
    output_dir = f"../output/coarse/{rt}/{parfile}/I{i}"
    macsys = createSystem(
        mesh_dir, mesh_name, parameter, tolerance, bounds, istate, cap
    )
    Idch = 1.0 / i
    t_dch = Qdch / Idch * h
    run(
        macsys,
        output_dir,
        -Idch * cap,
        t_dch=t_dch,
        #    cha=(tcha, 2.45, Icha * cap),
        t_pau=tpau,
    )


files = [
    "1S",
    "2S1A_1",
    "2S1A_2",  #'2S1A_3','2S1A_4',
    "2S2A_2",
    "2S2A_3",  #'2S2A_4','2S2A_5','2S2A_1',
    "ha_1",
    "ha_4",  #'ha_2','ha_3','ha_5',
]
idch = [20, 10, 5, 40]
pool = multiprocessing.Pool(16)
pool.map(runC, product(files, idch))
