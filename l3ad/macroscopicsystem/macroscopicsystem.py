import json
import os
import sys

import dolfin
from electrode.active_mass import PbActiveMass
from electrode.active_mass import PbO2ActiveMass
from electrode.electrode import NegativeElectrode
from electrode.electrode import PositiveElectrode
from electrode.electrontransport import ElectronTransport
from electrode.terminal import Terminal
from electrolyte.electrolyte import Electrolyte
from helper.domainvalue import DomainAssigner
from pbion.pbionconcentration import PbIonConcentration
from problem.flowproblem import FlowProblem
from problem.macroproblem import MACROProblem


class Macroscopicsystem:
    def __init__(
        self, meshinformation, statespace, id_dict, parameter, tolerances, bounds
    ):
        self.comm = dolfin.MPI.comm_world
        mpi_comm = self.comm

        self.parameter = parameter

        self.id_dict = id_dict
        self.statespace = statespace
        self.T = 298.15  # dolfin.Constant(298.15,name='T')
        self.meshinformation = meshinformation
        self._createFunctions(meshinformation, statespace)
        self._createSubSystems(meshinformation, statespace, parameter)
        #        self._flowproblem = FlowProblem(self.electrolyte.flowequation,
        #                                        statespace.flow)
        self._macroproblem = MACROProblem(
            models=[
                self.PAM,
                self.NAM,
                self.electrontransport,
                self.pbionconcentration,
                self.electrolyte.iontransport,
                self.electrolyte.masstransport,
                self.terminal,
            ],
            meshinformation=meshinformation,
            statespace=statespace,
            tolerances=tolerances["macro"],
            bounds=bounds,
            # flowproblem=self.flowproblem,
            solver_parameter=parameter.get("solver", None),
        )

        self.porosity_file = "parameter/porosity.xdmf"
        self.comm.barrier()

    def dumpparameter(self, outdir):
        if self.comm.rank == 0:
            with open("%s/parameter.json" % outdir, "w") as fp:
                json.dump(self.parameter, fp, indent=4)

    @property
    def flowproblem(self):
        return self._flowproblem

    @property
    def macroproblem(self):
        return self._macroproblem

    def _createFunctions(self, meshinformation, statespace):
        mesh = meshinformation.mesh
        cell_domains = meshinformation.celldomain
        namedict = statespace.namedict
        W0 = statespace.u[namedict["X"]].function_space()
        id_dict = self.id_dict
        self.porosity = dolfin.interpolate(dolfin.Constant(1.0), W0)
        tmp = dolfin.interpolate(dolfin.Constant(0.0), W0)
        DomainAssigner(
            cell_domains, W0, mesh, [id_dict["positive grid"], id_dict["negative grid"]]
        ).assign(self.porosity, tmp)

    def _createSubSystems(self, meshinformation, statespace, parameter):
        id_dict = self.id_dict
        porosity = self.porosity
        mesh = meshinformation.mesh
        cell_domains = meshinformation.celldomain
        dx = meshinformation.dx
        dx_am_p = dx(id_dict["positive active mass"])
        dx_am_n = dx(id_dict["negative active mass"])

        self.PAM = PbO2ActiveMass(
            dx=dx_am_p,
            meshinformation=meshinformation,
            statespace=statespace,
            elec_porosity=porosity,
            T=self.T,
            parameter=parameter,
            Id=id_dict["positive active mass"],
        )

        self.NAM = PbActiveMass(
            dx=dx_am_n,
            meshinformation=meshinformation,
            statespace=statespace,
            elec_porosity=porosity,
            T=self.T,
            parameter=parameter,
            Id=id_dict["negative active mass"],
        )

        self.positiveelectrode = PositiveElectrode(
            meshinformation=meshinformation,
            statespace=statespace,
            AM=self.PAM,
            parameter=parameter,
            id_grid=id_dict["positive grid"],
            id_terminal=id_dict["positive terminal"],
        )

        self.negativeelectrode = NegativeElectrode(
            meshinformation=meshinformation,
            statespace=statespace,
            AM=self.NAM,
            parameter=parameter,
            id_grid=id_dict["negative grid"],
            id_terminal=id_dict["negative terminal"],
        )
        self.electrontransport = ElectronTransport(
            meshinformation=meshinformation,
            statespace=statespace,
            electrodep=self.positiveelectrode,
            electroden=self.negativeelectrode,
        )

        self.terminal = Terminal(
            statespace=statespace,
            meshinformation=meshinformation,
            id_pos=id_dict["positive terminal"],
            id_neg=id_dict["negative terminal"],
            i_set=0.0,
        )

        self.electrolyte = Electrolyte(
            id_dict=id_dict,
            PAM=self.PAM,
            NAM=self.NAM,
            statespace=statespace,
            meshinformation=meshinformation,
            porosity=porosity,
            T=self.T,
            parameter=parameter,
        )

        self.pbionconcentration = PbIonConcentration(
            meshinformation,
            statespace,
            PAM=self.PAM,
            NAM=self.NAM,
            porosity=porosity,
            T=self.T,
            parameter=parameter,
        )

    def setTerminalCurrent(self, I):
        b = self.terminal.set_terminal_potential
        self.terminal.setTerminalCurrent(I)
        if b != self.terminal.set_terminal_potential:
            self.macroproblem.setupSolver()

    def setTerminalVoltage(self, U):
        b = self.terminal.set_terminal_potential
        self.terminal.setTerminalVoltage(U)
        if b != self.terminal.set_terminal_potential:
            self.macroproblem.setupSolver()

    def calculateSteadyState(self, first=False):
        self.macroproblem.calculateStS(first)

    @property
    def uelec(self):
        return self.electrolyte.getReferencePotential()

    @property
    def uterminal(self):
        return self.terminal.getTerminalVoltage()

    @property
    def iterminal(self):
        return self.terminal.getTerminalCurrent()

    @property
    def ict(self):
        return (self.PAM.getIct(), self.NAM.getIct())

    def setCpbtoSol(self, f=1):
        namedict = self.statespace.namedict
        cpb = self.statespace.u[namedict["cpb"]]
        self.pbionconcentration.setCtoSol(cpb, f)
        self.statespace.u.apply("from subfunctions")

    def initialise(self):
        self.macroproblem.calculateCoefficients(self.statespace.u)
        self.comm.barrier()

    def output_parameter(self, t, outdir):
        self.electrontransport.output_parameter(t, outdir)
        self.PAM.output_parameter(t, outdir)
        self.NAM.output_parameter(t, outdir)
        with dolfin.XDMFFile(self.comm, os.path.join(outdir, self.porosity_file)) as f:
            f.parameters["rewrite_function_mesh"] = False
            f.write_checkpoint(self.porosity, "porosity", t, append=True)
