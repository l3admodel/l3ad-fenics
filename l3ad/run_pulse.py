import json
import os
import shutil
import sys

import dolfin
import multiphenics as mp

dolfin.parameters["ghost_mode"] = "shared_facet"
dolfin.parameters["form_compiler"]["optimize"] = True
dolfin.parameters["form_compiler"]["cpp_optimize_flags"] = "-O2"
dolfin.parameters["linear_algebra_backend"] = "PETSc"

import petsc4py

petsc4py.init(sys.argv)

import numpy as np
from simdata.createTestSystem import createSystem
from simdata.runpulse import run
import multiprocessing
from itertools import product, chain
import helper.dictupdate as du

import helper.constants as con

mpi_comm = dolfin.MPI.comm_world
with open("tolerance.json") as fp:
    tolerance = json.load(fp)
with open("bounds.json") as fp:
    bounds = json.load(fp)
mpi_comm.barrier()

initstates = {
    "cacid": {
        "u0": 4.5,
        #        'uy': -0.4
    },
    "X": {
        "+": {
            "u0": 0.99,
        },
        "-": {
            "u0": 0.99,
            #            'uy': 0.15
        },
    },
    "n": {
        "+": {
            "u0": 1e3,
        },
        "-": {
            #'u0': 300,
            "u0": 1e3,
            #            'uy': -300,
        },
    },
}
full = True
if full:
    cap = 1.0
    mesh_dir = "../DissTestCell"
    mesh_name = "Cell1Ah"
else:
    cap = 2.0 * 0.5 / 36.0
    mesh_dir = "../SmallCellh"
    mesh_name = "CellCoarse"

h = 3600.0
d = h * 24.0

parameterfolder = "parameter1Ah"


def runC(pars):
    parfile = pars
    with open("parameter.json") as json_file:
        parameter = json.load(json_file)
    with open(f"{parameterfolder}/parameter_{parfile}.json") as json_file:
        parameterup = json.load(json_file)
    parameter = du.update(parameter, parameterup)
    istate = initstates.copy()
    output_dir = f"../output/pulse/{parfile}"
    macsys = createSystem(
        mesh_dir, mesh_name, parameter, tolerance, bounds, istate, cap
    )
    Qdch = 0.1
    Idch = 1.0 / 20.0
    t_dch = Qdch / Idch * h
    pulse = [(-Idch * cap, t_dch, 2 * h)]
    dt = 100
    for i in [0.015, 0.05, 0.15]:
        pulse.append((-cap * i, dt, dt))
        pulse.append((cap * i, dt, dt))
    pulse.append((-cap * 0.5, dt, dt))
    run(macsys, output_dir, pulse=pulse)


files = ["1S", "1Snodl"]
#        '2S1A_1','2S1A_2',#'2S1A_3','2S1A_4',
#       '2S2A_2','2S2A_3',#'2S2A_4','2S2A_5','2S2A_1',
#      'ha_1','ha_4',#'ha_2','ha_3','ha_5',
#     ]
pool = multiprocessing.Pool(16)
pool.map(runC, files)
