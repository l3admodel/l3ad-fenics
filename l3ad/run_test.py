import os
import pathlib
import shutil
import sys

import dolfin
import multiphenics as mp

# dolfin.set_log_level(10)

dolfin.parameters["ghost_mode"] = "shared_facet"
dolfin.parameters["form_compiler"]["optimize"] = True
dolfin.parameters["form_compiler"]["cpp_optimize_flags"] = "-O2"
dolfin.parameters["linear_algebra_backend"] = "PETSc"

import petsc4py

petsc4py.init(sys.argv)

import numpy as np
from helper.parameter import parameter
from helper.tolerance import getTolerance
from helper.bounds import getBounds
from simdata.createTestSystem import createSystem
from simdata.runnucvar import run
from pbion.crystal import amin, Nmin

mpi_comm = dolfin.MPI.comm_world

Nscale = parameter["scale"]["N"]
cascale = parameter["scale"]["cacid"]
cpbscale = parameter["scale"]["cpb"]
tolerance = getTolerance(parameter)
bounds = getBounds(parameter)
mpi_comm.barrier()

initstates = {
    "cacid": {"u0": 4500.0 / cascale},
    "X": {"+": {"u0": 0.9}, "-": {"u0": 0.9}},
    "r": {
        "+": {
            "u0": 700,
        },
        "-": {
            "u0": 700,
        },
    },
}

cap = 2.2 * 0.5 / 36.0
mesh_dir = "../SmallCellh"
mesh_name = "Cell"

h = 3600.0
d = h * 24.0

cap = 2.5 * 0.5 / 36
Qdch = 0.1
tcha = 2 * h
Icha = 1.0 / 20
tpau = 10 * d


def runC(i=20):
    output_dir = "../output/outputC%d" % i
    macsys = createSystem(
        mesh_dir, mesh_name, parameter, tolerance, bounds, initstates, 2.2 * 0.5 / 36
    )
    macsys.meshinformation.saveMeshData("%s/mesh" % output_dir, "mesh")
    Idch = 1.0 / i
    t_dch = Qdch / Idch * h
    run(
        macsys,
        output_dir,
        -Idch * cap,
        t_dch=t_dch,
        #        cha=(tcha, 2.45, Icha * cap),
        t_pau=tpau,
    )


runC(20)
runC(2)
