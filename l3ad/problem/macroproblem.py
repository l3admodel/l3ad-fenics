import dolfin
import multiphenics as mp
from problem.problem import BlockProblem
from timeintegration.tsmacro import TSMACRO


class MACROProblem(BlockProblem):
    def __init__(
        self,
        models,
        meshinformation,
        statespace,
        tolerances,
        bounds,
        solver_parameter=dict(),
    ):
        self.meshinformation = meshinformation
        self.mesh = meshinformation.mesh
        self.comm = self.mesh.mpi_comm()
        self.models = models
        self.statespace = statespace
        self.tolerances = tolerances
        self.bounds = bounds
        self._output = []
        self.parameter = solver_parameter

        self.timeintegrator = TSMACRO(self, self.statespace, self.comm)

    def __str__(self):
        return "MACRO Problem"

    def sett(self, t):
        [M.sett(t) for M in self.models]

    def appendoutput(self, out):
        self._output.append(out)

    def _getBoundaries(self):
        V = self.statespace.V
        lb = mp.BlockFunction(V)
        ub = mp.BlockFunction(V)
        state = self.statespace.types
        n = len(lb)
        for i in range(n):
            s = state[i]
            mp.assign(
                lb[i],
                dolfin.interpolate(dolfin.Constant(self.bounds[s]["lower"]), V.sub(i)),
            )
            mp.assign(
                ub[i],
                dolfin.interpolate(dolfin.Constant(self.bounds[s]["upper"]), V.sub(i)),
            )
        return (lb, ub)

    def _setupErrorCalculation(self):
        V = self.statespace.V
        atol = mp.BlockFunction(V)
        rtol = mp.BlockFunction(V)
        state = self.statespace.types

        for i in range(len(atol)):
            s = state[i]
            _atol = dolfin.interpolate(
                dolfin.Constant(self.tolerances[s]["absolute"]), V.sub(i)
            )
            mp.assign(atol[i], _atol)
            _rtol = dolfin.interpolate(
                dolfin.Constant(self.tolerances[s]["relative"]), V.sub(i)
            )
            mp.assign(rtol[i], _rtol)
        self.timeintegrator.setTolerances(
            atol.block_vector().vec(), rtol.block_vector().vec()
        )

    def _F(self, u, v, udot):
        FM = [M.F(u=u, udot=udot, v=v) for M in self.models]
        return [sum(f) for f in zip(*FM)]

    def _HSS(self, u, u0, v):
        FM = [M.F(u=u, u0=u0, v=v) for M in self.models]
        return [sum(f) for f in zip(*FM)]

    def setupSolver(self):
        self.timeintegrator.createSolver(
            F=self._F, bounds=self._getBoundaries(), parameter=self.parameter
        )
        self._setupErrorCalculation()

    def _initu(self, u):
        [M.initu(u) for M in self.models]
        u.apply("from subfunctions")

    def calculateStS(self, first=False):
        V = self.statespace.V
        u0 = self.statespace.u
        v = mp.BlockTestFunction(V)
        u = mp.BlockFunction(V)
        du = mp.BlockTrialFunction(V)
        mp.block_assign(u, u0)

        self.calculateCoefficients(u)

        self.sett(self.t)

        G = mp.BlockForm(self._HSS(u, u0, v))
        J = mp.block_derivative(G, u, du)
        problem = mp.BlockNonlinearProblem(G, u, None, J)
        solver = mp.BlockPETScSNESSolver(problem)
        solver_parameters = {
            "nonlinear_solver": "snes",
            "snes_solver": {
                "linear_solver": "mumps",
                "maximum_iterations": 500,
                "line_search": "basic",
                "relative_tolerance": 1e-18,
                "report": True,
                "error_on_nonconvergence": True,
            },
        }
        solver.parameters.update(solver_parameters["snes_solver"])
        solver.solve()
        mp.block_assign(self.statespace.u, u)
        self.timeintegrator.setSolution(u)

    def initSolution(self):
        self.timeintegrator.setSolution(self.statespace.u)

    #    def solve(self, t):
    #        try:
    #            super().solve(t)
    #        except:
    #            print("Forcing Output")
    #            self.output(self.t, True)
    #            raise
    #
    def output(self, t, force=False):
        for o in self._output:
            o(t, force)
        self.comm.barrier()

    def calculateCoefficients(self, u):
        [M.update(u) for M in self.models]

    def initialise(self):
        self.calculateCoefficients(self.statespace.u)
        self._initu(self.statespace.u)
        self._initu(self.statespace.u)
