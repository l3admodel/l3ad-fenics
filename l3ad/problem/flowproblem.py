import dolfin
import helper.constants as con
import multiphenics as mp
import numpy as np
from steadystatesolve.kspflow import KSPFlow


class FlowProblem:
    def __init__(self, equation, statespace):
        self.equation = equation
        self.statespace = statespace
        self.setupSolver()

    def __str__(self):
        return "Flow Problem"

    def update(self, *args):
        self.solver.update(*args)

    def solve(self):
        self.solver.solve()

    def integrateToTtarget(self, *args):
        self.solve()

    def setupSolver(self):
        self.solver = KSPFlow(self.statespace)
        self.solver.createSolver(
            self.equation._A, self.equation._b, self.equation._bc  # None)
        )

    def calculateIC(self):
        #    if dolfin.MPI.comm_world.rank == 0:
        #      print("%s: calculateIC" % self)
        self.solve()
