class BlockProblem:
    def update(self, **kwargs):
        self.timeintegrator.update(**kwargs)

    def setTimeStep(self, h):
        self.timeintegrator.h = h

    def doTimeStep(self):
        cr = self.timeintegrator.doStep()

    def integrateToTtarget(self, t_target):
        cr = self.timeintegrator.integrateToTtarget(t_target)

    def solve(self, ttarget):
        if self.t < ttarget:
            self.integrateToTtarget(ttarget)

    @property
    def timeintegrator(self):
        return self._timeintegrator

    @timeintegrator.setter
    def timeintegrator(self, ti):
        self._timeintegrator = ti

    @property
    def h(self):
        h = self.timeintegrator.h
        return h

    @h.setter
    def h(self, _h):
        self.timeintegrator.h = _h

    @property
    def t(self):
        t = self.timeintegrator.t
        return t

    @t.setter
    def t(self, _t):
        self.timeintegrator.t = _t

    @property
    def tmax(self):
        t = self.timeintegrator.tmax
        return t

    @tmax.setter
    def tmax(self, _t):
        self.timeintegrator.tmax = _t

    def restartStep(self):
        self.timeintegrator.restartStep()

    def reset(self):
        self.timeintegrator.reset()

    def setEvent(self, eventhandler):
        self.timeintegrator.setEventHandler(eventhandler)
