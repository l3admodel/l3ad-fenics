import sys

import dolfin
import multiphenics as mp
from steadystatesolve.petscksp import crmap
from steadystatesolve.petscksp import PETScKSP


class KSPFlow(PETScKSP):
    def __init__(self, statespace, comm=dolfin.MPI.comm_world):
        super().__init__(statespace, comm)
        self.ksp.setType(self.ksp.Type.PREONLY)
        self.ksp.setFromOptions()
        pc = self.ksp.getPC()
        pc.setType(pc.Type.LU)
        pc.setFactorSolverType("mumps")
        pc.setFactorOrdering(reuse=True)
        self.bc = None

    def createSolver(self, _A, _b, _bc):
        V = self.statespace.V
        v = mp.BlockTestFunction(V)
        du = mp.BlockTrialFunction(V)
        self.A_form = mp.BlockForm(_A(du, v))
        self.b_form = mp.BlockForm(_b(v))
        self.A = mp.block_assemble(self.A_form)
        self.b = mp.block_assemble(self.b_form)
        if _bc:
            self.bc = mp.BlockDirichletBC(_bc(V))
        if self.bc is not None:
            self.bc.apply(self.A)
        self.ksp.setOperators(self.A.mat(), self.A.mat())

    def update(self):
        mp.block_assemble(self.A_form, block_tensor=self.A)
        if self.bc is not None:
            self.bc.apply(self.A)
        mp.block_assemble(self.b_form, block_tensor=self.b)
        self.ksp.setOperators(self.A.mat(), self.A.mat())

    def solve(self, *args):
        self.update()
        if self.bc is not None:
            self.bc.apply(self.b)
        self.ksp.solve(self.b.vec(), self.statespace.u.block_vector().vec())
        self.statespace.u.apply("to subfunctions")
        cr = self.ksp.getConvergedReason()
        if cr < 0:
            raise RuntimeError("Failed to solve flowproblem (%s)" % crmap[cr])
