import dolfin
import multiphenics as mp
import petsc4py.PETSc
from petsc4py.PETSc import KSP

crmap = {
    1: "KSP_CONVERGED_RTOL_NORMAL",
    9: "KSP_CONVERGED_ATOL_NORMAL",
    2: "KSP_CONVERGED_RTOL",
    3: "KSP_CONVERGED_ATOL",
    4: "KSP_CONVERGED_ITS",
    5: "KSP_CONVERGED_CG_NEG_CURVE",
    6: "KSP_CONVERGED_CG_CONSTRAINED",
    7: "KSP_CONVERGED_STEP_LENGTH",
    8: "KSP_CONVERGED_HAPPY_BREAKDOWN",
    -2: "KSP_DIVERGED_NULL",
    -3: "KSP_DIVERGED_ITS",
    -4: "KSP_DIVERGED_DTOL",
    -5: "KSP_DIVERGED_BREAKDOWN",
    -6: "KSP_DIVERGED_BREAKDOWN_BICG",
    -7: "KSP_DIVERGED_NONSYMMETRIC",
    -8: "KSP_DIVERGED_INDEFINITE_PC",
    -9: "KSP_DIVERGED_NANORINF",
    -10: "KSP_DIVERGED_INDEFINITE_MAT",
    -11: "KSP_DIVERGED_PCSETUP_FAILED",
}


class PETScKSP:
    def __init__(self, statespace, comm):
        self.comm = comm
        self.statespace = statespace
        self.ksp = KSP().create(comm)

    @property
    def statespace(self):
        return self._statespace

    @statespace.setter
    def statespace(self, x):
        if hasattr(self, "_statespace"):
            raise AttributeError("StateSpace already set")
        self._statespace = x

    def setTolerances(self, atol=None, rtol=None, divtol=None):
        self.ts.setTolerances(rtol, atol, divtol)

    def _retrieveSolution(self):
        s = self.ksp.getSolution()
        s.copy(self._getVector(self.statespace.u).vec())
        self._getVector(self.statespace.u).apply("add")
        if isinstance(self.statespace.u, mp.BlockFunction):
            self.statespace.u.apply("to subfunctions")
