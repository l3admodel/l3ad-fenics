import dolfin
import multiphenics as mp
import numpy as np

eps = np.finfo(float).eps


class SNESBoundChecker:
    def __init__(self, lb, ub=None):
        self.lb = lb
        self.ub = ub

    def __call__(self, xvec, yvec):
        changed = 0
        #    print("Calling Bound Checker")
        xarr = xvec.getArray(True)
        yarr = yvec.getArray()
        lb = self.lb.vec().getArray(True)
        low = (yarr + xarr) < lb  # +np.abs(lb)*eps)
        if np.any(low):
            print("Correcting step", sum(low))
            #      print(xarr[low])
            yarr[low] = lb[low] - xarr[low]  # +np.abs(lb[low])*eps
            changed = 1

        #    if self.ub is not None:
        #      ub=self.ub.vec().getArray(True)
        #      high=(xarr+yarr)>ub-eps
        #      yarr[high]=ub[high]-xarr[high]
        #      changed=1

        return changed
