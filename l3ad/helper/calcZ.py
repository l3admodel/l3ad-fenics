import math

import numpy as np


def calcImpedanceUsingFFT(curr_vec, volt_vec, periods, newSampleNumb):
    complexMagn_vec = np.fft.fft(curr_vec, newSampleNumb) / newSampleNumb
    complexCurr_vec = complexMagn_vec[0 : round(newSampleNumb / 2)]
    complexCurr_vec[1:-2] = 2 * complexCurr_vec[1:-2]
    complexCurr_vec = complexCurr_vec[0:-1:periods]

    complexMagn_vec = np.fft.fft(volt_vec, newSampleNumb) / newSampleNumb
    complexVolt_vec = complexMagn_vec[0 : round(newSampleNumb / 2)]
    complexVolt_vec[1:-2] = 2 * complexVolt_vec[1:-2]
    complexVolt_vec = complexVolt_vec[0:-1:periods]

    complexZ = complexVolt_vec[1] / complexCurr_vec[1]
    realZ = complexZ.real
    imgZ = complexZ.imag
    return realZ, imgZ
