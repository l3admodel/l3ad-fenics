import dolfin
import ufl
from dolfin import avg
from dolfin import Constant
from dolfin import dot
from dolfin import grad
from dolfin import inner
from dolfin import jump

# DOI 10.1137/080719583
alpha = Constant(4)


def harmonicmean(x):
    return 2.0 * x("+") * x("-") / (x("+") + x("-"))


def advdiff_A(
    u, v, a, b, dx, dS, meshinformation, theta=dolfin.Constant(-1), dirichlet=None
):
    n = meshinformation.n
    h = meshinformation.h

    def upwind(tau):
        alpha = (ulf.sign(dot(b * n)) + 1) / 2
        return avg(tau) + jump(alpha) * jump(tau) / 2

    sigma_f = alpha / avg(h) * harmonicmean(a)
    return (
        a * dot(grad(u), grad(v)) * dx
        - dot(b * u, grad(v)) * dx
        - harmonicmean(a) * dot(upwind(grad(u)), n) * jump(v) * dS
        - theta * harmonicmean(a) * dot(upwind(grad(v)), n) * jump(u) * dS
        + sigma_f * jump(u) * jump(v) * dS
        + dot(upwind(b * u), n) * jump(v) * dS
    )


def advdiff_ADirichletBC(u, v, a, ds, meshinformation, theta=dolfin.Constant(-1)):
    n = meshinformation.n
    h = meshinformation.h
    return -(a * dot(grad(u), n) * v + theta * a * dot(grad(v), n) * u) * ds


def advdiff_KDirichletBC(v, a, ds, gD, meshinformation, theta=dolfin.Constant(-1)):
    n = meshinformation.n
    h = meshinformation.h

    sigma_f = alpha / avg(h) * harmonicmean(a)
    return -gD * (sigma_f * jump(v) - theta * a * grad(v)) * ds


def advdiff_KNeumannBC(v, ds, gN):
    return -gN * v * ds
