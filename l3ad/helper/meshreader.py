import os
import pathlib
import sys

import dolfin
import multiphenics as mp
import numpy as np
import ufl


class MeshReader:
    def __init__(self, mesh_dir, mesh_name, id_dict):

        self.mesh_dir = mesh_dir
        self.mesh_name = mesh_name
        self.id_dict = id_dict

    @property
    def mesh(self):
        if not hasattr(self, "_mesh"):
            self._mesh = dolfin.Mesh(dolfin.MPI.comm_world)
            mesh_dir = self.mesh_dir
            mesh_name = self.mesh_name
            mesh = self._mesh
            with dolfin.XDMFFile(
                mesh.mpi_comm(), os.path.join(mesh_dir, mesh_name + ".xdmf")
            ) as f:
                f.read(mesh)
        return self._mesh

    @property
    def celldomain(self):
        if not hasattr(self, "cell_domain"):
            mesh_dir = self.mesh_dir
            mesh_name = self.mesh_name
            mesh = self.mesh
            self.cell_domain = dolfin.MeshFunction(
                "size_t", mesh, mesh.topology().dim()
            )
            with dolfin.XDMFFile(
                mesh.mpi_comm(),
                os.path.join(mesh_dir, mesh_name + "_physical_region.xdmf"),
            ) as f:
                f.read(self.cell_domain)
        return self.cell_domain

    @property
    def facetdomain(self):
        if not hasattr(self, "facet_domain"):
            mesh_dir = self.mesh_dir
            mesh_name = self.mesh_name
            mesh = self.mesh
            self.facet_domain = dolfin.MeshFunction(
                "size_t", mesh, mesh.topology().dim() - 1
            )
            with dolfin.XDMFFile(
                mesh.mpi_comm(),
                os.path.join(mesh_dir, mesh_name + "_facet_region.xdmf"),
            ) as f:
                f.read(self.facet_domain)
        return self.facet_domain

    @property
    def electroderestriction(self):
        if not hasattr(self, "electrode_restriction"):
            mesh_dir = self.mesh_dir
            mesh_name = self.mesh_name
            mesh = self.mesh
            self.electrode_restriction = mp.MeshRestriction(
                mesh, os.path.join(mesh_dir, "electrode_restriction.rtc.xdmf")
            )
        return self.electrode_restriction

    @property
    def positiveelectroderestriction(self):
        if not hasattr(self, "positive_electrode_restriction"):
            mesh_dir = self.mesh_dir
            mesh_name = self.mesh_name
            mesh = self.mesh
            self.positive_electrode_restriction = mp.MeshRestriction(
                mesh, os.path.join(mesh_dir, "positive_electrode_restriction.rtc.xdmf")
            )
        return self.positive_electrode_restriction

    @property
    def terminalrestriction(self):
        if not hasattr(self, "terminal_restriction"):
            mesh_dir = self.mesh_dir
            mesh_name = self.mesh_name
            mesh = self.mesh
            self.terminal_restriction = mp.MeshRestriction(
                mesh, os.path.join(mesh_dir, "terminal_restriction.rtc.xdmf")
            )
        return self.terminal_restriction

    @property
    def positiveterminalrestriction(self):
        if not hasattr(self, "positive_terminal_restriction"):
            mesh_dir = self.mesh_dir
            mesh_name = self.mesh_name
            mesh = self.mesh
            self.positive_terminal_restriction = mp.MeshRestriction(
                mesh, os.path.join(mesh_dir, "positive_terminal_restriction.rtc.xdmf")
            )
        return self.positive_terminal_restriction

    @property
    def negativeelectroderestriction(self):
        if not hasattr(self, "negative_electrode_restriction"):
            mesh_dir = self.mesh_dir
            mesh_name = self.mesh_name
            mesh = self.mesh
            self.negative_electrode_restriction = mp.MeshRestriction(
                mesh, os.path.join(mesh_dir, "negative_electrode_restriction.rtc.xdmf")
            )
        return self.negative_electrode_restriction

    @property
    def electrolyterestriction(self):
        if not hasattr(self, "electrolyte_restriction"):
            mesh_dir = self.mesh_dir
            mesh_name = self.mesh_name
            mesh = self.mesh
            self.electrolyte_restriction = mp.MeshRestriction(
                mesh, os.path.join(mesh_dir, "electrolyte_restriction.rtc.xdmf")
            )
        return self.electrolyte_restriction

    @property
    def freeelectrolyterestriction(self):
        if not hasattr(self, "free_electrolyte_restriction"):
            mesh_dir = self.mesh_dir
            mesh_name = self.mesh_name
            mesh = self.mesh
            self.free_electrolyte_restriction = mp.MeshRestriction(
                mesh, os.path.join(mesh_dir, "free_electrolyte_restriction.rtc.xdmf")
            )
        return self.free_electrolyte_restriction

    @property
    def activemassrestriction(self):
        if not hasattr(self, "am_restriction"):
            mesh_dir = self.mesh_dir
            mesh_name = self.mesh_name
            mesh = self.mesh
            self.am_restriction = mp.MeshRestriction(
                mesh, os.path.join(mesh_dir, "activemass_restriction.rtc.xdmf")
            )
        return self.am_restriction

    @property
    def nactivemassrestriction(self):
        if not hasattr(self, "nam_restriction"):
            mesh_dir = self.mesh_dir
            mesh_name = self.mesh_name
            mesh = self.mesh
            self.nam_restriction = mp.MeshRestriction(
                mesh, os.path.join(mesh_dir, "nam_restriction.rtc.xdmf")
            )
        return self.nam_restriction

    @property
    def positiveshell(self):
        if not hasattr(self, "positive_shell"):
            mesh_dir = self.mesh_dir
            mesh_name = self.mesh_name
            mesh = self.mesh
            self.positive_shell = dolfin.MeshFunction("size_t", mesh, 2)
            with dolfin.XDMFFile(
                mesh.mpi_comm(), os.path.join(mesh_dir, "positive_shell.xdmf")
            ) as f:
                f.read(self.positive_shell)
        return self.positive_shell

    @property
    def electrodeshell(self):
        if not hasattr(self, "electrode_shell"):
            mesh_dir = self.mesh_dir
            mesh_name = self.mesh_name
            mesh = self.mesh
            self.electrode_shell = dolfin.MeshFunction("size_t", mesh, 2)
            with dolfin.XDMFFile(
                mesh.mpi_comm(), os.path.join(mesh_dir, "electrode_shell.xdmf")
            ) as f:
                f.read(self.electrode_shell)
        return self.electrode_shell

    @property
    def negativeshell(self):
        if not hasattr(self, "negative_shell"):
            mesh_dir = self.mesh_dir
            mesh_name = self.mesh_name
            mesh = self.mesh
            self.negative_shell = dolfin.MeshFunction("size_t", mesh, 2)
            with dolfin.XDMFFile(
                mesh.mpi_comm(), os.path.join(mesh_dir, "negative_shell.xdmf")
            ) as f:
                f.read(self.negative_shell)
        return self.negative_shell

    @property
    def electrolyteshell(self):
        if not hasattr(self, "electrolyte_shell"):
            mesh_dir = self.mesh_dir
            mesh_name = self.mesh_name
            mesh = self.mesh
            self.electrolyte_shell = dolfin.MeshFunction("size_t", mesh, 2)
            with dolfin.XDMFFile(
                mesh.mpi_comm(), os.path.join(mesh_dir, "electrolyte_shell.xdmf")
            ) as f:
                f.read(self.electrolyte_shell)
        return self.electrolyte_shell

    @property
    def freeelectrolyteshell(self):
        if not hasattr(self, "freeelectrolyte_shell"):
            mesh_dir = self.mesh_dir
            mesh_name = self.mesh_name
            mesh = self.mesh
            self.freeelectrolyte_shell = dolfin.MeshFunction("size_t", mesh, 2)
            with dolfin.XDMFFile(
                mesh.mpi_comm(), os.path.join(mesh_dir, "freeelectrolyte_shell.xdmf")
            ) as f:
                f.read(self.freeelectrolyte_shell)
        return self.freeelectrolyte_shell

    @property
    def dx(self):
        return dolfin.Measure("dx", domain=self.mesh, subdomain_data=self.celldomain)

    @property
    def dx_e(self):
        return self.dx(self.id_dict["electrolyte"])

    #    @property
    #    def dx_s(self):
    #        return self.dx(self.id_dict["separator"])

    @property
    def dx_pam(self):
        return self.dx(self.id_dict["positive active mass"])

    @property
    def dx_nam(self):
        return self.dx(self.id_dict["negative active mass"])

    @property
    def dx_ngrid(self):
        return self.dx(self.id_dict["negative grid"])

    @property
    def dx_pgrid(self):
        return self.dx(self.id_dict["positive grid"])

    @property
    def ds(self):
        return dolfin.Measure("ds", domain=self.mesh, subdomain_data=self.facetdomain)

    @property
    def dS(self):
        return dolfin.Measure("dS", domain=self.mesh, subdomain_data=self.facetdomain)

    @property
    def n(self):
        return dolfin.FacetNormal(self.mesh)

    @property
    def h(self):
        return dolfin.CellDiameter(self.mesh)

    def saveMeshData(self, mesh_dir_out, mesh_name):
        mesh = self.mesh
        comm = mesh.mpi_comm()
        if comm.rank == 0:
            pathlib.Path(mesh_dir_out).mkdir(exist_ok=True, parents=True)
        dolfin.XDMFFile(
            mesh.mpi_comm(), os.path.join(mesh_dir_out, mesh_name + ".xdmf")
        ).write(self.mesh)
        #    phys = dolfin.MeshValueCollection('size_t', mesh=self.mesh)
        #    phys.assign(self.cell_domain)
        #    dolfin.XDMFFile(
        #        mesh.mpi_comm(),
        #        os.path.join(mesh_dir_out,
        #                     mesh_name + "_physical_region.xdmf")).write(phys)
        p = dolfin.File(
            mesh.mpi_comm(),
            os.path.join(mesh_dir_out, mesh_name + "_physical_region.pvd"),
        )
        p << self.cell_domain

        #    facet = dolfin.MeshValueCollection('size_t', mesh=self.mesh)
        #    facet.assign(self.facet_domain)
        #    dolfin.XDMFFile(
        #        mesh.mpi_comm(),
        #        os.path.join(mesh_dir_out,
        #                     mesh_name + "_facet_region.xdmf")).write(facet)
        f = dolfin.File(
            mesh.mpi_comm(), os.path.join(mesh_dir_out, mesh_name + "_facet_region.pvd")
        )
        f << self.facet_domain
        if comm.size > 1:
            tmp = dolfin.MeshFunction("size_t", self.mesh, self.mesh.topology().dim())
            tmp.set_all(comm.rank)
            #      dom = dolfin.MeshValueCollection('size_t', mesh=self.mesh)
            #      dom.assign(tmp)
            #      dolfin.XDMFFile(mesh.mpi_comm(),
            #                      os.path.join(mesh_dir_out,
            #                                   mesh_name + "_partition.xdmf")).write(dom)
            t = dolfin.File(
                mesh.mpi_comm(),
                os.path.join(mesh_dir_out, mesh_name + "_partition.pvd"),
            )
            t << tmp
        es = dolfin.File(
            mesh.mpi_comm(),
            os.path.join(mesh_dir_out, mesh_name + "_electrolyteshell.pvd"),
        )
        es << self.electrolyteshell

    @property
    def restrictions(self):
        if not hasattr(self, "_restrictions"):
            self._restrictions = {
                "electrodes": self.electroderestriction,
                "positiveelectrode": self.positiveelectroderestriction,
                "negativeelectrode": self.negativeelectroderestriction,
                "electrolyte": self.electrolyterestriction,
                "activemass": self.activemassrestriction,
                "negativeactivemass": self.nactivemassrestriction,
                "terminal": self.terminalrestriction,
            }
        return self._restrictions

    @property
    def elements(self):
        if not hasattr(self, "_elements"):
            self._elements = {
                "Q0": dolfin.FiniteElement("DG", self.mesh.ufl_cell(), 0),
                "Q1": dolfin.FiniteElement("DG", self.mesh.ufl_cell(), 1),
            }
        return self._elements

    @property
    def functionspaces(self):
        if not hasattr(self, "_functionspaces"):
            self._functionspaces = {
                "Q0": dolfin.FunctionSpace(self.mesh, self.elements["Q0"]),
                "Q1": dolfin.FunctionSpace(self.mesh, self.elements["Q1"]),
            }
        return self._functionspaces
