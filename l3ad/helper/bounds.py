import numpy as np
from pbion.crystal import amin
from pbion.crystal import Nmin


def getBounds(parameter):
    Nscale = parameter["scale"]["N"]
    cascale = parameter["scale"]["cacid"]
    cpbscale = parameter["scale"]["cpb"]
    return {
        "u": {"lower": -np.inf, "upper": np.inf},
        "q": {"lower": -np.inf, "upper": np.inf},
        "cpb": {"lower": 1e-18 / cpbscale, "upper": 1e1 / cpbscale},
        "r": {"lower": 0.98 * amin, "upper": 1e6},
        "n": {"lower": 0.98 * Nmin / Nscale, "upper": 1e30 / Nscale},
        "ca": {"lower": 1.0 / cascale, "upper": 1e4 / cascale},
        "X": {"lower": 0, "upper": 1.0},
        "theta": {"lower": 0, "upper": 1.0},
        "it": {"lower": -np.inf, "upper": np.inf},
    }
