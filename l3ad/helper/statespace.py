import os
import pathlib
import sys

import dolfin
import multiphenics as mp
import numpy as np
from helper.printdofmap import block_owned_dofs_local_numbering


class StateSpaceOutput:
    def __init__(self, comm, statespace, output_dir, dt, t0=-np.inf):
        self.comm = comm
        self.tlast = t0
        self.outdir = os.path.join(output_dir, "state")
        outpath = pathlib.Path(self.outdir)
        outpath.mkdir(exist_ok=True)
        self.dt = dt
        self.statespace = statespace

    def __call__(self, t, force=False):
        if (t - self.tlast > self.dt) or (force and t != self.tlast):
            self.statespace.output(t, self.outdir, self.comm)
            self.tlast = t


class StateSpace:
    def __init__(self, meshinformation, names, VRT, input_dict=None, udot=True):
        self.names = names
        self._createTypes(names, VRT)
        self._createFunctionSpace(meshinformation, names, VRT)
        self._createFunctions(udot)

    #        if input_dict:
    #            print("Reading  StateSpace")
    #            inpdir = input_dict["dir"]
    #            n = input_dict["n"]
    #            self._readData(self, "%s" % inpdir, n)
    #
    def _createTypes(self, names, VRT):
        self.types = [VRT[n][2] for n in names]

    def output(self, t, outdir, comm, app=True):
        for n, u in zip(self.names, mp.block_split(self.u)):
            if n:
                filename = os.path.join(outdir, "%s.xdmf" % n)
                with dolfin.XDMFFile(comm, filename) as f:
                    f.parameters["rewrite_function_mesh"] = False
                    f.parameters["functions_share_mesh"] = True
                    f.write_checkpoint(u, n, t, append=app)
        comm.barrier()

    @property
    def u(self):
        return self._u

    @property
    def udot(self):
        return self._udot

    @property
    def V(self):
        return self._V

    @u.setter
    def u(self, x):
        if not hasattr(self, "_u"):
            self._u = x
        else:
            raise AttributeError("Attribute already assigend")

    @udot.setter
    def udot(self, x):
        if not hasattr(self, "_udot"):
            self._udot = x
        else:
            raise AttributeError("Attribute already assigend")

    @V.setter
    def V(self, x):
        if not hasattr(self, "_V"):
            self._V = x
        else:
            raise AttributeError("Attribute already assigend")

    def _createFunctionSpace(self, meshinformation, names, VR):
        mesh = meshinformation.mesh

        R = [meshinformation.restrictions[VR[n][1]] for n in names]
        V = [meshinformation.elements[VR[n][0]] for n in names]
        self.V = mp.BlockFunctionSpace(mesh, V, restrict=R)
        self.namedict = dict(zip(names, range(len(names))))

    #        for k, v in self.namedict.items():
    #            dof = block_owned_dofs_local_numbering(self.V, v)
    #            print(k, min(dof), max(dof))

    def _createFunctions(self, udot):
        V = self.V
        self.u = mp.BlockFunction(V)
        for n, u in zip(self.names, mp.block_split(self.u)):
            u.rename("%s" % n, "%s" % n)

        if udot:
            self.udot = mp.BlockFunction(V)
            for n, u in zip(self.names, mp.block_split(self.udot)):
                u.rename("%sdot" % n, "%sdot" % n)


#    def _readData(self, statespace, indir, n=-1):
#        u = statespace.u
#        _list = statespace.names
#        assert (len(u) == len(_list))
#        for i, name in zip(range(len(u)), _list):
#            if name:
#                tmp = dolfin.Function(u.sub(i).function_space())
#                filename = os.path.join(indir, "%s.xdmf" % name)
#                with dolfin.XDMFFile(dolfin.MPI.comm_world, filename) as f:
#                    f.read_checkpoint(tmp, name, n)
#                mp.assign(u[i], dolfin.project(tmp, u.sub(i).function_space()))
#        u.apply("from subfunctions")

#    @property
#    def macro(self):
#        return self._macro
#
#    @property
#    def flow(self):
#        return self._flow
#
#    def _createFunctionsFlow(self):
#        V = self._flow.V
#        self._flow.u = mp.BlockFunction(V)
#
#    def _createFunctionSpaceFlow(self, meshinformation):
#        mesh = meshinformation.mesh
#        VE = dolfin.FiniteElement("CG", mesh.ufl_cell(), 2)
#        P = dolfin.FiniteElement("CG", mesh.ufl_cell(), 1)
#        Q = dolfin.FiniteElement("DG", mesh.ufl_cell(), 0)
#        PQ = P + Q
#        V = dolfin.VectorElement(VE)
#        elr = meshinformation.freeelectrolyterestriction
#        self.flow.V = mp.BlockFunctionSpace(mesh, [V, PQ], restrict=[elr, elr])
#        self._flow.names = ["v", "p"]
#        self._pout = dolfin.FunctionSpace(mesh, P)
#        self._poutdg = dolfin.FunctionSpace(mesh, Q)
#
#        def output(t, outpath, comm, app=True):
#            _self = self._flow
#            n = _self.names[0]
#            u = _self.u[0]
#            filename = os.path.join(outpath, "%s.xdmf" % n)
#            with dolfin.XDMFFile(comm, filename) as f:
#                f.write_checkpoint(u, n, t, append=app)
#            u = dolfin.project(dolfin.div(_self.u[0]), self._poutdg)
#            filename = os.path.join(outpath, "%s_div.xdmf" % n)
#            with dolfin.XDMFFile(comm, filename) as f:
#                f.write_checkpoint(u, n, t, append=app)
#            n = _self.names[1]
#            u = dolfin.project(_self.u[1], self._pout)
#            filename = os.path.join(outpath, "%s.xdmf" % n)
#            with dolfin.XDMFFile(comm, filename) as f:
#                f.write_checkpoint(u, n, t, append=app)
#
#        self._flow.output = output
