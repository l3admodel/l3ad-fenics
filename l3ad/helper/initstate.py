import dolfin


def initstate(
    V, initstates, coords={"x0": 0, "x1": 1, "y0": 0, "y1": 1, "z0": 0, "z1": 1}
):
    initargs = {"u0": initstates.get("u0")}
    initstring = "u0"
    for c in ["x", "y", "z"]:
        if c == "x":
            k = 0
        elif c == "y":
            k = 1
        elif c == "z":
            k = 2
        if f"u{c}" in initstates.keys():
            initargs.update(
                {f"{c}0": coords.get(f"{c}0", 0), f"{c}1": coords.get(f"{c}1", 1)}
            )
            uc = initstates[f"u{c}"]
            if not isinstance(uc, list):
                uc = [uc]
            for i in range(len(uc)):
                initstring += f"+std::pow((x[{k}]-{c}0)/{c}1*2-1,{i+1:d})*u{c}_{i+1:d}"
                initargs.update({f"u{c}_{i+1:d}": uc[i]})

    #    print(initstring,initargs)
    return dolfin.Expression(initstring, element=V.ufl_element(), **initargs)
