import dolfin
from helper.modelblock import ModelBlock
from helper.poisson_dg import PoissonDG


class Transport(ModelBlock):
    def __init__(self, meshinformation, statespace, dx, ds, shell, statename, alpha=1):
        super().__init__(statespace, meshinformation)

        self.statename = statename
        dS = dolfin.Measure(
            "dS", domain=self.meshinformation.mesh, subdomain_data=shell
        )
        self.poisson = PoissonDG(
            dx=dx, dS=dS(0), ds=ds, meshinformation=meshinformation, alpha=alpha
        )

    def F(self, u, v, udot=None, u0=None):
        nd = self.statespace.namedict
        f = [0] * len(u)
        f[nd[self.statename]] = self._Fu(u, v, udot)
        return f

    def ustate(self, u):
        nd = self.statespace.namedict
        return u[nd[self.statename]]

    def vstate(self, v):
        nd = self.statespace.namedict
        return v[nd[self.statename]]

    def _Fu(self, u, v, udot=None):
        v = self.vstate(v)
        g = self._Auu(self.ustate(u), v)
        g += self._S(u, udot, v)
        g += self._BC(u, v)
        return g

    def _Auu(self, u, v):
        return self.poisson.A(u=u, v=v, a=self.phi)
