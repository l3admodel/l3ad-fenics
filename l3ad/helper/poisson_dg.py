import dolfin
import ufl
from dolfin import avg
from dolfin import Constant
from dolfin import dot
from dolfin import grad
from dolfin import inner
from dolfin import jump

# DOI 10.1137/080719583


def facetvalue(x):
    # return ufl.Min(x('+'), x('-'))
    return 2.0 * x("+") * x("-") / (x("+") + x("-"))


class PoissonDG:
    def __init__(self, dx, dS, ds, meshinformation, theta=-1, alpha=1):
        self.dx = dx
        self.dS = dS
        self.ds = ds
        self.meshinformation = meshinformation
        self.theta = dolfin.Constant(theta)
        self.alpha = dolfin.Constant(alpha)

    def sigma(self, a):
        h = self.meshinformation.h
        return self.alpha / avg(h) * facetvalue(a)

    def A(self, u, v, a):
        n = self.meshinformation.n
        sigma_f = self.sigma(a)
        dx = self.dx
        dS = self.dS
        return (
            a * dot(grad(u), grad(v)) * dx
            - facetvalue(a) * avg(dot(grad(u), n)) * jump(v) * dS
            - self.theta * facetvalue(a) * avg(dot(grad(v), n)) * jump(u) * dS
            + sigma_f * jump(u) * jump(v) * dS
        )

    def ADirichletBC(self, u, v, a):
        n = self.meshinformation.n
        return (
            -(a * dot(grad(u), n) * v + self.theta * a * dot(grad(v), n) * u) * self.ds
        )

    def KDirichletBC(self, v, a, gD):
        n = self.meshinformation.n
        sigma_f = self.sigma(a)
        return -gD * (sigma_f * jump(v) - self.theta * a * grad(v)) * self.ds

    def KNeumannBC(self, v, gN):
        return -gN * v * self.ds
