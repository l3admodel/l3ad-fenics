import numpy as np


def getTolerance(parameter):
    Nscale = parameter["scale"]["N"]
    cascale = parameter["scale"]["cacid"]
    cpbscale = parameter["scale"]["cpb"]
    return {
        "macro": {
            "u": {"relative": 1e-1, "absolute": 1e-2},
            "q": {"relative": 1e-3, "absolute": 1},
            "cpb": {"relative": 1e-3, "absolute": 1e-15 / cpbscale},
            "r": {"relative": 1e-3, "absolute": 1e-2},
            "n": {"relative": 1e-3, "absolute": 1e9 / Nscale},
            "ca": {"relative": 1e-3, "absolute": 1e-1 / cascale},
            "X": {"relative": 1e-3, "absolute": 1e-3},
            "it": {"relative": 1, "absolute": np.inf},
            "theta": {"relative": 1e-3, "absolute": 1e-3},
        }
    }
