import dolfin
import scipy.constants as con

# https://docs.scipy.org/doc/scipy/reference/constants.html#module-scipy.constants

R = con.gas_constant
F = con.physical_constants["Faraday constant"][0]
g = con.g
N = con.N_A
k_B = con.k
e = con.e
st = con.Stefan_Boltzmann
atm = 101325.0
