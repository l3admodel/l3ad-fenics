import dolfin


class Coefficient:
    def __init__(self, name, function_space, namedict):
        self.name = name
        self.k = dolfin.Function(function_space)
        self.k.rename(name, name)
        self.namedict = namedict

    def update(self, u):
        raise NotImplementedError

    @property
    def value(self):
        return self.k
