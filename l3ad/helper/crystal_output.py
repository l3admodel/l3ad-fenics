import csv
import pathlib
import sys


class CrystalOutput:
    def __init__(self, output_dir, comm, cPAM, cNAM):
        self.comm = comm
        self.cPAM = cPAM
        self.cNAM = cNAM
        if self.comm.rank == 0:
            self.output_dir = output_dir
            outpath = pathlib.Path(output_dir)
            outpath.mkdir(exist_ok=True)
            with open("%s/crystal.csv" % self.output_dir, "w") as f:
                ui_csv = csv.writer(f, delimiter="\t")
                ui_csv.writerow(["t", "rp", "np", "rn", "nn", "Sp", "Sn"])

    def write(self, row):
        if self.comm.rank == 0:
            with open("%s/crystal.csv" % self.output_dir, "a") as f:
                ui_csv = csv.writer(f, delimiter="\t")
                ui_csv.writerow(row)

    def __call__(self, t, *args):
        rp = self.cPAM.ravg
        rn = self.cNAM.ravg
        np = self.cPAM.navg
        nn = self.cNAM.navg
        Sp = self.cPAM.Savg
        Sn = self.cNAM.Savg
        self.write(
            [
                f"{t:0.12e}",
                f"{rp:0.9e}",
                f"{np:0.12e}",
                f"{rn:0.9e}",
                f"{nn:0.12e}",
                f"{Sp:0.9e}",
                f"{Sn:0.9e}",
            ]
        )
