import numpy as np


class ParameterOutput:
    def __init__(self, sys, outdir, dt, t0=-np.inf):
        self.tlast = t0
        self.outdir = outdir
        self.dt = dt
        self.sys = sys

    def __call__(self, t, *args):
        if t - self.tlast > self.dt:
            self.sys.output_parameter(t, self.outdir)
            self.tlast = t
