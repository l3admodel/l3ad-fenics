import csv
import pathlib
import sys


class UI_Output:
    def __init__(self, output_dir, comm, macsys, header, name="ui", disp=False):
        self.comm = comm
        self.macsys = macsys
        self.disp = disp
        header = ["t", "u", "ue", "un", "i", "i_ct_p", "i_ct_n"]
        if self.comm.rank == 0:
            self.output_dir = output_dir
            if output_dir:
                outpath = pathlib.Path(output_dir)
                outpath.mkdir(exist_ok=True, parents=True)
                self.name = name
                with open(f"{self.output_dir}/{name}.csv", "w") as ui_file:
                    ui_csv = csv.writer(ui_file, delimiter="\t")
                    ui_csv.writerow(header)

    def write(self, row):
        if self.output_dir:
            with open(f"{self.output_dir}/{self.name}.csv", "a") as f:
                ui_csv = csv.writer(f, delimiter="\t")
                ui_csv.writerow(row)

    def __call__(self, t, _):
        u = self.macsys.uterminal
        u_elec = self.macsys.uelec
        i = self.macsys.iterminal
        i_ct = self.macsys.ict
        if self.comm.rank == 0:
            self.write(
                [
                    "%.12e" % t,
                    "%.5e" % (u[0] - u[1]),
                    "%.5e" % (u_elec - u[1]),
                    "%.5e" % u[1],
                    "%.5e" % i,
                    "%.5e" % i_ct[0],
                    "%.5e" % i_ct[1],
                ]
            )
            if self.disp:
                print(
                    "t: %.12e\nU:%.5e (%e)\nI:%.5e\nI_ct_pos:%.5e\tI_ct_neg:%.5e"
                    % (t, u[0] - u[1], u_elec - u[1], i, i_ct[0], i_ct[1])
                )
                sys.stdout.flush()


class UI_Store:
    def __init__(self, macsys):
        self.macsys = macsys
        self.t = []
        self.U = []
        self.Upos = []
        self.Uneg = []
        self.I = []

    def __call__(self, t, *args):
        u = self.macsys.uterminal
        u_elec = self.macsys.uelec
        i = self.macsys.iterminal
        self.t.append(t)
        self.I.append(i)
        self.U.append(u[0] - u[1])
        self.Upos.append(u[0] - u_elec)
        self.Uneg.append(u_elec - u[1])
