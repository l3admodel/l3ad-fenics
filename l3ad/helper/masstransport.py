from helper.transport import Transport


class MassTransport(Transport):
    def __init__(
        self,
        meshinformation,
        statespace,
        PAM,
        NAM,
        porosity,
        T,
        shell,
        statename,
        alpha=1,
    ):

        self.PAM = PAM
        self.NAM = NAM
        dx_e = meshinformation.dx_e
        super().__init__(
            meshinformation=meshinformation,
            statespace=statespace,
            dx=self.PAM.dx + self.NAM.dx + dx_e,
            ds=None,
            shell=shell,
            statename=statename,
            alpha=alpha,
        )
        self.dx_e = dx_e
        self.porosity = porosity
        self.T = T

    def initu(self, *args):
        pass

    def F(self, u, v, udot=None, u0=None):
        nd = self.statespace.namedict
        f = [0] * len(u)
        f[nd[self.statename]] = self._Fc(u=u, v=v, udot=udot, u0=u0)
        return f

    def _Fc(self, u, v, udot=None, u0=None):
        nd = self.statespace.namedict
        c = u[nd[self.statename]]
        vc = v[nd[self.statename]]
        if udot:
            cdot = udot[nd[self.statename]]
            g = self._Mc(cdot, vc)
            g += self._Acc(c, vc)
            g += self._S(u, udot, vc)
            return g
        elif u0:
            c0 = u0[nd[self.statename]]
            return self._Mc(c - c0, vc)

    def _Mc(self, cdot, v):
        g = self.porosity * cdot * v * self.PAM.dx
        g += self.porosity * cdot * v * self.NAM.dx
        g += cdot * v * self.dx_e
        return g

    def _Acc(self, c, v):
        return self.poisson.A(c, v, self.D)

    def _S(self, u, udot, v):
        nd = self.statespace.namedict
        c = u[nd[self.statename]]
        g = c * self.PAM.activemass.porosity_am.Pdot(udot) * v * self.PAM.dx
        g += c * self.NAM.activemass.porosity_am.Pdot(udot) * v * self.NAM.dx
        return g
