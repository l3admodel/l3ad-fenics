import os

import multiphenics
from dolfin import *
from multiphenics import *

cpp_code = """
    #include <pybind11/pybind11.h>
    #include <pybind11/stl.h>
    #include <multiphenics/function/BlockFunctionSpace.h>

    std::vector<dolfin::la_index> block_owned_dofs__local_numbering(std::shared_ptr<multiphenics::BlockFunctionSpace> block_function_space, std::size_t component)
    {
        return block_function_space->block_dofmap()->block_owned_dofs__local_numbering(component);
    }

    PYBIND11_MODULE(SIGNATURE, m)
    {
        m.def("block_owned_dofs__local_numbering", &block_owned_dofs__local_numbering);
    }
"""
multiphenics_dir = os.path.abspath(
    os.path.join(os.path.dirname(os.path.abspath(multiphenics.__file__)), "..")
)
cpp_module = compile_cpp_code(cpp_code, include_dirs=[multiphenics_dir])


def block_owned_dofs_local_numbering(block_function_space, component):
    return cpp_module.block_owned_dofs__local_numbering(
        block_function_space._cpp_object, component
    )
