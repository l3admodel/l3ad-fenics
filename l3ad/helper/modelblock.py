class ModelBlock:
    def __init__(self, statespace, meshinformation):
        self.statespace = statespace
        self.meshinformation = meshinformation

    def update(self, u):
        pass

    def F(self, u, v, udot=None, u0=None):
        raise NotImplementedError()

    def initu(self, u):
        pass

    def sett(self, t):
        pass
