import dolfin
import helper.constants as con
import multiphenics as mp
import ufl


class ZeroOut:
    def __init__(self, meshinformation):
        self.meshinformation = meshinformation

    def zeroi_ct(self, i_ct, v):
        dx = (
            self.meshinformation.dx_e
            + self.meshinformation.dx_s
            + self.meshinformation.dx_ngrid
            + self.meshinformation.dx_pgrid
        )
        return i_ct * v * dx

    def zerocpb(self, c, v):
        return 0
        dx_grid = self.meshinformation.dx_pgrid + self.meshinformation.dx_ngrid
        return c * v * dx_grid
