import dolfin

# class DomainValue:
#
#    def __init__(self, domains, V, mesh, value_on, value_off, ids, **kwargs):
#        dofmap = V.dofmap()
#        self.on_dofs = []
#        self.off_dofs = []
#
#        for cell in dolfin.cells(mesh):
#            cell_id = domains[cell]
#            if (isinstance(ids, int)):
#                is_in = cell_id == ids
#            else:
#                is_in = (cell_id in ids)
#            if is_in:
#                self.on_dofs.extend(dofmap.cell_dofs(cell.index()))
#            else:
#                self.off_dofs.extend(dofmap.cell_dofs(cell.index()))
#
#        self.value = [value_off, value_on]
#        self.ids = ids
#
#    def assign(self, u):
#        u.vector()[self.off_dofs] = self.value[0]
#        u.vector()[self.on_dofs] = self.value[1]


class DomainAssigner:
    def __init__(self, domains, V, mesh, ids, **kwargs):
        dofmap = V.dofmap()
        self.on_dofs = []

        for cell in dolfin.cells(mesh):
            cell_id = domains[cell]
            if isinstance(ids, int):
                is_in = cell_id == ids
            else:
                is_in = cell_id in ids
            if is_in:
                self.on_dofs.extend(dofmap.cell_dofs(cell.index()))

    def assign(self, u_to, u_from):
        v = u_to.vector().get_local()
        if isinstance(u_from, float):
            v[self.on_dofs] = u_from
        elif hasattr(u_from, "vector"):
            v[self.on_dofs] = u_from.vector().get_local()[self.on_dofs]
        else:
            raise TypeError("Expected float or function")
        u_to.vector().set_local(v)
        u_to.vector().apply("insert")

    def __call__(self, u_to, u_from):
        self.assign(u_to, u_from)

    @property
    def dof(self):
        return self.on_dofs
