import numbers
import queue

import dolfin
import multiphenics as mp
import numpy as np


class Event:
    def __init__(self, init, postevent, terminate=False, crossing=0):
        self._init = init
        self._postevent = postevent
        self._finished = False
        self.terminate = terminate
        self.crossing = crossing

    def finished(self, ts, t, *arg):
        return self._finished

    def init(self, ts, t, uvec):
        if self._init:
            self._init(ts, t, uvec)

    def postevent(self, *args):
        self._finished = True
        if self._postevent:
            self._postevent(*args)


class LowerBoundaryEvent(Event):
    def __init__(self, lb, init=None, postevent=None):
        super().__init__(init, postevent)
        self.crossing = -1
        lbarr = lb.vec().getArray(True)
        self.idx = np.where(np.isfinite(lbarr))
        self.lb = lbarr[self.idx]

    def __call__(self, ts, t, uvec):
        uarr = uvec.getArray(True)
        bvec = np.divide(uarr[self.idx] - self.lb, np.abs(self.lb))
        b = np.min(bvec)
        return b

    def finished(self, ts, t, *arg):
        return self._finished

    def init(self, ts, t, uvec):
        pass

    def postevent(self, *args):
        super().postevent(*args)


class TimeEvent(Event):
    def __init__(
        self, dt, t0=None, init=None, postevent=None, terminate=False, crossing=1
    ):
        super().__init__(
            init=init, postevent=postevent, terminate=terminate, crossing=crossing
        )
        self.t0 = t0
        self.dt = dt

    def __call__(self, ts, t, *arg):
        if not self.t0:
            return -self.crossing
        temp = t - (self.t0 + self.dt)
        return temp

    def finished(self, ts, t, *arg):
        return self._finished

    def init(self, ts, t, uvec):
        if not self.t0:
            self.t0 = t
        super().init(ts, t, uvec)
        # print("init TimeEvent at %f for %f from %f" % (t, self.dt, self.t0))

    def postevent(self, *args):
        self._finished = True
        super().postevent(*args)


class EisEvent(Event):
    def __init__(
        self, f, n, k, eisoutput, init, postevent, terminate=False, crossing=1
    ):
        super().__init__(
            init=init, postevent=postevent, terminate=terminate, crossing=crossing
        )
        self.n = n
        self.f = f
        self.k = k
        self.output = eisoutput

    @property
    def dt(self):
        return 1.0 / ((self.n - 1) * self.f)

    def __call__(self, ts, t, *arg):
        if self._finished == True:
            return self.crossing
        temp = t - (self.tlast + self.dt)
        return temp

    def finished(self, ts, t, *arg):
        return self._finished

    def init(self, ts, t, uvec):
        hmin, hmax = ts.getStepLimits()
        ts.setStepLimits(hmin, self.dt)
        self.hmaxold = hmax
        self.hminold = hmin
        self.t0 = t
        self.tlast = t
        self.tend = self.dt * (self.k * (self.n - 1)) + self.t0
        #        print("init eis event at %f to %f (%f)" % (t, self.tend, self.dt))
        ts.setStepLimits(self.hminold, self.dt)
        super().init(ts, t, uvec)
        self.output(t - self.t0)

    def postevent(self, ts, t, uvec):
        self.output(t - self.t0)
        self.tlast = t
        self._finished = t >= self.tend
        if self._finished == True:
            ts.setStepLimits(self.hminold, self.hmaxold)
            super().postevent(ts, t, uvec)


class EmptyQue(Event):
    def __init__(self, eventque, terminate=True, crossing=-1):
        super().__init__(None, None, terminate, crossing)
        self.que = eventque

    def __call__(self, ts, t, uvec):
        return float(len(self.que)) - 0.5


class UEvent(Event):
    def __init__(
        self,
        U,
        macsys,
        crossing=1,
        terminate=True,
        tolerance=1e-4,
        init=None,
        postevent=None,
    ):
        super().__init__(init, postevent, terminate)
        self.U = U
        self.crossing = crossing
        self._macsys = macsys
        self._tolerance = tolerance

    @property
    def tolerance(self):
        return self._tolerance

    def __call__(self, ts, t, uvec):
        if self._finished == True:
            tmp = self.crossing
        else:
            u = self._macsys.uterminal
            tmp = u[0] - u[1] - self.U
        return tmp

    def postevent(self, *args):
        self._finished = True
        super().postevent(*args)


class EventQue(Event):
    def __init__(self, crossing=1, terminate=False):
        super().__init__(None, self._postevent, terminate, crossing)
        self.actualevent = None
        self.events = queue.Queue()

    def __call__(self, *args):
        if self.actualevent:
            tmp = self.actualevent(*args)
        else:
            tmp = self.crossing
        return tmp

    def __len__(self):
        if self.actualevent:
            return 1 + self.events.qsize()
        else:
            return 0

    def add(self, event):
        self.events.put(event)
        if not self.actualevent:
            self.actualevent = self.events.get()
            self.actualevent.init(None, 0.0, None)

    def popevent(self, ts, t, uvec):
        if not self.events.empty():
            self.actualevent = self.events.get()
            self.actualevent.init(ts, t, uvec)
        else:
            self.actualevent = None

    def _postevent(self, ts, t, uvec):
        if self.actualevent:
            self.actualevent.postevent(ts, t, uvec)
            if self.actualevent.finished(ts, t, uvec):
                self.popevent(ts, t, uvec)


class EventHandler:
    def __init__(self, tolerance=None):
        self.events = []
        self.updatestate = False
        self._tol = tolerance

    def __call__(self, ts, t, uvec):
        return [e(ts, t, uvec) for e in self.events]

    def add(self, event):
        self.events.append(event)

    def postevent(self, ts, eventszero, t, uvec, forwardsolve=False):
        for idx in eventszero:
            self.events[idx].postevent(ts, t, uvec)

    def initevents(self, ts, t, uvec):
        [e.init(ts, t, uvec) for e in self.events]

    @property
    def tolerance(self):
        return self._tol

    @property
    def n(self):
        return len(self.events)

    @property
    def crossing(self):
        return [e.crossing for e in self.events]

    @property
    def terminate(self):
        return [e.terminate for e in self.events]


class EventHelper:
    def __init__(self, macsys):
        self.macsys = macsys


class SetI(EventHelper):
    def __init__(self, Inew, macsys):
        super().__init__(macsys)
        self.Inew = Inew

    def __call__(self, ts, t, uvec):
        self.macsys.setTerminalCurrent(self.Inew)


class ConstI(SetI):
    def __init__(self, Inew, macsys):
        assert isinstance(Inew, numbers.Number)
        super().__init__(Inew, macsys)


class ConstU(EventHelper):
    def __init__(self, Unew, macsys):
        super().__init__(macsys)
        self.Unew = Unew

    def __call__(self, ts, t, uvec):
        self.macsys.setTerminalVoltage(self.Unew)


class NewDt(EventHelper):
    def __init__(self, dt):
        self.dt = dt

    def __call__(self, ts, t, uvec):
        ts.setPostEventIntervalStep(self.dt)
