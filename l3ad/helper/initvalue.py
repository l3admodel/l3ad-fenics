import dolfin


class InitValue(dolfin.Expression):
    def __init__(self, mesh, domains, on_value, off_value, ids, **kwargs):
        self.mesh = mesh
        self.domains = domains
        self.ids = ids
        self.value_on = on_value
        self.value_off = off_value

    def eval(self, value, x):
        cell_ids = self.domains.array()[
            self.mesh.bounding_box_tree().compute_entity_collisions(dolfin.Point(x))
        ]
        if any(x in self.ids for x in cell_ids):
            value[0] = self.value_on
        else:
            value[0] = self.value_off
