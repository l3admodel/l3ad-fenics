import os

from helper.meshreader import MeshReader
from helper.statespace import StateSpaceCollection
from readdomains import readdomains

mesh_dir = "../AddESunMesh"

mesh_name = "Cell"
indir = "../output"
outdir = "../input"
indict = {"dir": indir, "n": -1}

domains = readdomains(os.path.join(mesh_dir, mesh_name + ".msh"))
id_dict = {
    "positive terminal": domains["PTerm"],
    "positive grid": domains["PGrid"],
    "positive active mass": domains["PAM"],
    "negative terminal": domains["NTerm"],
    "negative grid": domains["NGrid"],
    "negative active mass": domains["NAM"],
    "electrolyte": domains["Electrolyte"],
    # "separator": domains["Separator"],
    "electrolyte reference": domains["ElectrolyteReference"],
}
# , "case": domains["Case"]}

meshinformation = MeshReader(mesh_dir, mesh_name, id_dict)
statespace = StateSpaceCollection(meshinformation, outdir, indict)
statespace.output(0, False)
