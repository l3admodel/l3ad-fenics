import json
import os
import pathlib
import shutil

import dolfin
import multiphenics as mp
import numpy as np
import ufl
from helper.domainvalue import DomainAssigner
from helper.initstate import initstate
from helper.meshreader import MeshReader
from helper.statespace import StateSpace
from macroscopicsystem.macroscopicsystem import Macroscopicsystem
from readdomains import readdomains

macro_names = [
    "us",
    "ue",
    "q",
    "cpb",
    "r",
    "n",
    "ca",
    "X",
    "it"
    #    "us", "ue",  "q","theta", "cpb", "r", "n", "ca", "X", "it"
    #   "us", "ue",  "q","theta1","theta2", "cpb", "r", "n", "ca", "X", "it"
]
macro_VRT = {
    "us": ("Q0", "electrodes", "u"),
    "ue": ("Q0", "electrolyte", "u"),
    "ul": ("Q0", "negativeactivemass", "u"),
    "q": ("Q0", "activemass", "q"),
    "theta": ("Q0", "negativeactivemass", "theta"),
    "theta1": ("Q0", "negativeactivemass", "theta"),
    "theta2": ("Q0", "negativeactivemass", "theta"),
    "cpb": ("Q0", "electrolyte", "cpb"),
    "r": ("Q0", "activemass", "r"),
    "n": ("Q0", "activemass", "n"),
    "ca": ("Q0", "electrolyte", "ca"),
    "X": ("Q0", "activemass", "X"),
    "it": ("Q0", "terminal", "it"),
    "cpbpore": ("Q0", "activemass", "cpb"),
    "capore": ("Q0", "activemass", "capore"),
}


def createSystem(mesh_dir, mesh_name, parameter, tolerance, bounds, initstates, cap):
    mpi_comm = dolfin.MPI.comm_world

    domains = readdomains(os.path.join(mesh_dir, mesh_name + ".msh"))
    parameter["capacity"] = cap
    id_dict = {
        "positive terminal": domains["PTerm"],
        "positive grid": domains["PGrid"],
        "positive active mass": domains["PAM"],
        "negative terminal": domains["NTerm"],
        "negative grid": domains["NGrid"],
        "negative active mass": domains["NAM"],
        "electrolyte": domains["Electrolyte"],
        "electrolyte reference": domains["ElectrolyteReference"],
    }
    meshinformation = MeshReader(mesh_dir, mesh_name, id_dict)
    coords = meshinformation.mesh.coordinates()
    xmin = np.inf
    ymin = np.inf
    zmin = np.inf
    xmax = -np.inf
    ymax = -np.inf
    zmax = -np.inf
    for xyz in coords:
        x = xyz[0]
        y = xyz[1]
        z = xyz[2]
        xmin = min(xmin, x)
        ymin = min(ymin, y)
        zmin = min(zmin, z)
        xmax = max(xmax, x)
        ymax = max(ymax, y)
        zmax = max(zmax, z)
    dx = xmax - xmin
    dy = ymax - ymin
    dz = zmax - zmin
    coords = {"x0": xmin, "x1": dx, "y0": ymin, "y1": dy, "z0": zmin, "z1": dz}

    adstates = set()
    par = parameter["PbO2"]
    if "adsorption" in par.keys():
        parAds = par["adsorption"]
        for key in parAds.keys():
            if key.startswith("theta"):
                adstates.add(key)
    par = parameter["Pb"]
    if "adsorption" in par.keys():
        parAds = par["adsorption"]
        for key in parAds.keys():
            if key.startswith("theta"):
                adstates.add(key)
    macronames = macro_names.copy()
    macronames.extend(sorted(adstates))
    print(adstates)
    print(macronames)

    statespace = StateSpace(meshinformation, macronames, macro_VRT)
    macsys = Macroscopicsystem(
        meshinformation=meshinformation,
        statespace=statespace,
        id_dict=id_dict,
        parameter=parameter,
        tolerances=tolerance,
        bounds=bounds,
    )
    macsys.cap = cap

    u_c = statespace.u[statespace.namedict["ca"]]
    c0 = initstate(u_c.function_space(), initstates["cacid"], coords)  # ,
    #   u_cpore = statespace.u[statespace.namedict["capore"]]
    c0Acid = dolfin.project(c0, u_c.function_space())
    ids = [
        id_dict["positive active mass"],
        id_dict["negative active mass"],
        id_dict["electrolyte"],
    ]
    DomainAssigner(
        meshinformation.celldomain, u_c.function_space(), meshinformation.mesh, ids
    ).assign(u_c, c0Acid)
    mpi_comm.barrier()

    ids = [id_dict["positive active mass"], id_dict["negative active mass"]]
    idr = statespace.namedict["r"]
    idn = statespace.namedict["n"]
    idx = statespace.namedict["X"]
    X = dolfin.Function(statespace.V.sub(idx))
    # Positive
    X0 = initstate(X.function_space(), initstates["X"]["+"], coords)
    dolfin.assign(X, dolfin.project(X0, X.function_space()))
    DomainAssigner(
        meshinformation.celldomain, X.function_space(), meshinformation.mesh, ids[0]
    ).assign(statespace.u[idx], X)

    X0 = initstate(X.function_space(), initstates["X"]["-"], coords)
    dolfin.assign(X, dolfin.project(X0, X.function_space()))
    DomainAssigner(
        meshinformation.celldomain, X.function_space(), meshinformation.mesh, ids[1]
    ).assign(statespace.u[idx], X)
    statespace.u.apply("from subfunctions")
    crystalPAM = macsys.pbionconcentration.crystalPAM
    crystalNAM = macsys.pbionconcentration.crystalNAM
    if "r" in initstates.keys():
        crystalPAM.initr(statespace.u[idr], initstates["r"]["+"], coords)
        crystalNAM.initr(statespace.u[idr], initstates["r"]["-"], coords)
        statespace.u.apply("from subfunctions")
        if "n" not in initstates.keys():
            crystalPAM.calculaten(statespace.u)
            crystalNAM.calculaten(statespace.u)
    elif "n" in initstates.keys():
        crystalPAM.initn(statespace.u[idn], initstates["n"]["+"], coords)
        crystalNAM.initn(statespace.u[idn], initstates["n"]["-"], coords)
        statespace.u.apply("from subfunctions")
        if "r" not in initstates.keys():
            crystalPAM.calculater(statespace.u)
            crystalNAM.calculater(statespace.u)
    statespace.u.apply("from subfunctions")

    macsys.PAM._cap = cap
    macsys.NAM._cap = cap
    return macsys
