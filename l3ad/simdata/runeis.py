import pathlib
import sys

import dolfin
import helper.petscevent as event
import multiphenics as mp
import numpy as np
from helper.parameteroutput import ParameterOutput
from helper.statespace import StateSpaceOutput
from helper.ui_output import UI_Output
from simdata.eisdata import eis
from simdata.eishelper import runEIS


def run(macsys, output_dir, ifac=1):
    mpi_comm = dolfin.MPI.comm_world
    outpath = pathlib.Path("%s" % output_dir)
    outpath.mkdir(parents=True, exist_ok=True)
    ui = UI_Output(
        output_dir,
        mpi_comm,
        macsys,
        ["t", "u", "ue", "un", "i", "i_ct_p", "i_ct_n"],
        disp=False,
    )
    problem = macsys.macroproblem
    problem.appendoutput(ui)
    outputstate = StateSpaceOutput(mpi_comm, macsys.statespace, output_dir, 500)
    problem.appendoutput(outputstate)
    mpi_comm.barrier()
    outpath_eis = pathlib.Path("%s/EIS" % output_dir)
    outpath_eis.mkdir(exist_ok=True)
    problem.setupSolver()
    mpi_comm.barrier()
    macsys.dumpparameter(output_dir)
    macsys.initialise()
    macsys.output_parameter(0, output_dir)
    macsys.setCpbtoSol(1.0)
    problem.initialise()
    problem.output(-1)
    cap = macsys.cap

    I20 = cap / 20
    mpi_comm.barrier()
    macsys.setTerminalCurrent(0)
    macsys.calculateSteadyState(True)
    problem.initialise()
    i = 1
    h = 3600.0
    problem.output(0)
    macsys.NAM.printAEC()

    #    print("Cycle 1")
    #    print("DCH")
    #    problem.timeintegrator.setEventHandler(None)
    #    macsys.setTerminalCurrent(-4 * I20)
    #    uevent = event.UEvent(1.75, macsys, crossing=-1)
    #    eventhandler = event.EventHandler(tolerance=1e-3)
    #    eventhandler.add(uevent)
    #    problem.timeintegrator.setEventHandler(eventhandler)
    #    problem.solve(5 * h)
    #    print("CC-CHA")
    #    macsys.setTerminalCurrent(I20)
    #    uevent = event.UEvent(2.45, macsys, crossing=1)
    #    eventhandler = event.EventHandler(tolerance=1e-3)
    #    eventhandler.add(uevent)
    #    problem.timeintegrator.setEventHandler(eventhandler)
    #    t0 = problem.t
    #    problem.solve(t0 + 24*h)
    #    if problem.t - t0 < 24 * h:
    #        print("CV-CHA for %eh" % (24 - (problem.t - t0) / h))
    #        problem.timeintegrator.setEventHandler(None)
    #        macsys.setTerminalVoltage(2.45)
    #        problem.solve(t0 + 24*h)

    #    print("Setting SoC")
    #    macsys.setTerminalCurrent(-4 * I20)
    #    problem.solve(problem.t + 0.5*h)
    # print("Starting Idle Phase")
    # macsys.setTerminalCurrent(0)
    # problem.solve(problem.t + 12*h)

    eisI20 = eis["mI20"]
    frequencies = eisI20["f"]
    I_ac = eisI20["Iac"] * cap
    I_dc = eisI20["Idc"] * cap
    runEIS(
        macsys=macsys,
        outdir=output_dir,
        problem=problem,
        frequencies=frequencies,
        I_dc=ifac * I_dc,
        I_ac=I_ac,
        nmbr=i,
        suffix="mI20",
    )
    macsys.dumpparameter(output_dir)
