import pathlib
import sys

import dolfin
import multiphenics as mp
import numpy as np
from helper.parameteroutput import ParameterOutput
from helper.statespace import StateSpaceOutput
from helper.ui_output import UI_Output
from simdata.eisdata import eis
from simdata.runeisvariation import runEIS


def runvariationeis(macsys):
    mpi_comm = dolfin.MPI.comm_world
    problem = macsys.macroproblem

    macsys.initialise()
    problem.initialise()
    macsys.setCpbtoSol(1.0)
    problem.initialise()
    problem.setupSolver()
    cap = macsys.cap

    def doEIS(frequencies, I_dc, I_ac):
        return runEIS(
            macsys=macsys,
            problem=problem,
            frequencies=frequencies,
            I_dc=I_dc,
            I_ac=I_ac,
        )

    I20 = cap / 20
    mpi_comm.barrier()
    # problem.output(-1)
    macsys.setTerminalCurrent(0)
    # macsys.flowproblem.calculateIC()
    macsys.calculateSteadyState(True)
    problem.initialise()
    mpi_comm.barrier()
    # caculateIC()
    i = 1
    h = 3600.0
    problem.output(0)
    problem.solve(2 * h)

    eisI20 = eis["mI20"]
    frequencies = eisI20["f"]
    I_ac = eisI20["Iac"] * cap
    I_dc = eisI20["Idc"] * cap
    return doEIS(frequencies, I_dc, I_ac)
