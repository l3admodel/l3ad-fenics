import os
import pathlib
import sys

import helper.petscevent as event
import numpy as np
from helper.ui_output import UI_Output


class OutputEis:
    def __init__(self, macsys, ui_eis):
        self.macsys = macsys
        self.ui_eis = ui_eis

    def __call__(self, t):
        mpi_comm = self.macsys.comm
        if mpi_comm.rank == 0:
            i = self.macsys.iterminal
            u = self.macsys.uterminal
            u_elec = self.macsys.uelec
            self.ui_eis.write(
                [
                    "%.12e" % t,
                    "%.9e" % (u[0] - u[1]),
                    "%.9e" % (u_elec - u[1]),
                    "%.9e" % i,
                ]
            )
        mpi_comm.barrier()


def runEIS(macsys, outdir, problem, frequencies, I_dc, I_ac, suffix, nmbr=0):
    mpi_comm = macsys.comm
    cap = macsys.cap
    pid = os.getpid()
    print("Starting EIS %s (%d) at %d " % (suffix, nmbr, pid))
    output_dir = "%s/EIS" % outdir
    if mpi_comm.rank == 0:
        outpath_eis = pathlib.Path(f"{output_dir}/{suffix}")
        outpath_eis.mkdir(exist_ok=True)
    t0 = float(problem.t)
    tdc = max(0.0, 3600.0 * 1e-2 * cap / abs(I_dc))
    print("Starting DC for %fs" % tdc)
    macsys.setTerminalCurrent(I_dc)
    problem.h = 1e-3
    problem.solve(t0 + tdc)
    n = 32.0
    k = 3.0
    dt = 1.0 / (frequencies[0] * n)
    timeisover = event.TimeEvent(dt=4 * tdc, t0=t0, terminate=True)
    for f, iac in zip(frequencies, I_ac):
        print("Starting EIS at %f Hz %f Adc %f Aac at %d" % (f, I_dc, iac, pid))
        ui_eis = UI_Output(
            output_dir,
            mpi_comm,
            macsys,
            ["t", "u", "ue", "i"],
            "/%s/eis_%d_%s_%fHz_%fA" % (suffix, nmbr, suffix, f, I_dc),
        )

        eisoutput = OutputEis(macsys, ui_eis)
        t0 = float(problem.t)
        t = float(problem.t)
        omega = 2.0 * np.pi * f
        _acfunc = lambda x: I_dc + iac * np.math.sin(omega * (x - t0))
        eventhandler = event.EventHandler()
        eisevent = event.EisEvent(
            f,
            n,
            k,
            eisoutput=eisoutput,
            init=event.SetI(_acfunc, macsys),
            postevent=event.ConstI(I_dc, macsys),
        )
        eventhandler.add(eisevent)
        eventhandler.add(timeisover)
        problem.setEvent(eventhandler)
        problem.h = eisevent.dt
        problem.solve(eisevent.tend)

    macsys.setTerminalCurrent(I_dc)
    t0 = float(problem.t)
    problem.solve(t0 + 300)
