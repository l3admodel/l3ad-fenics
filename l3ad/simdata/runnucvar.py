import pathlib
import sys

import dolfin
import helper.petscevent as event
import multiphenics as mp
import numpy as np
from helper.crystal_output import CrystalOutput
from helper.parameteroutput import ParameterOutput
from helper.statespace import StateSpaceOutput
from helper.ui_output import UI_Output


def run(macsys, output_dir, I, t_dch, t_pau=None, cha=None):
    mpi_comm = dolfin.MPI.comm_world

    ui = UI_Output(
        output_dir,
        mpi_comm,
        macsys,
        ["t", "u", "ue", "i", "i_ct_p", "i_ct_n"],
        disp=True,
    )
    problem = macsys.macroproblem
    problem.appendoutput(ui)
    #    outputstate = StateSpaceOutput(mpi_comm, macsys.statespace, output_dir,
    #                                   3600)
    #    problem.appendoutput(outputstate)
    #    parameterout = ParameterOutput(macsys, output_dir, 300)
    #    problem.appendoutput(parameterout)
    cout = CrystalOutput(
        output_dir,
        mpi_comm,
        macsys.pbionconcentration.crystalPAM,
        macsys.pbionconcentration.crystalNAM,
    )
    problem.appendoutput(cout)
    problem.setupSolver()
    mpi_comm.barrier()
    macsys.dumpparameter(output_dir)

    macsys.initialise()
    macsys.setCpbtoSol(1.0)
    problem.initialise()
    cap = macsys.cap

    mpi_comm.barrier()
    macsys.setTerminalCurrent(0)
    macsys.calculateSteadyState(True)
    problem.initialise()
    problem.output(0.0)
    macsys.setTerminalCurrent(I)
    problem.solve(problem.t + t_dch)
    if cha:
        t_cha, U_cha, I_cha = cha
        macsys.setTerminalCurrent(I_cha)
        problem.timeintegrator.h = 1e-3
        uevent = event.UEvent(U_cha, macsys)
        eventhandler = event.EventHandler()
        eventhandler.add(uevent)
        problem.timeintegrator.setEventHandler(eventhandler)
        t0 = problem.t
        problem.solve(t0 + t_cha)
        if problem.t - t0 < t_cha:
            macsys.setTerminalVoltage(U_cha)
            problem.solve(t0 + t_cha)

    if t_pau:
        macsys.setTerminalCurrent(0)
        problem.solve(problem.t + t_pau)
