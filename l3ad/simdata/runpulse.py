import pathlib
import sys

import dolfin
import helper.petscevent as event
import multiphenics as mp
import numpy as np
from helper.parameteroutput import ParameterOutput
from helper.statespace import StateSpaceOutput
from helper.ui_output import UI_Output


def run(macsys, output_dir, pulse, outputstate=None):
    mpi_comm = dolfin.MPI.comm_world

    ui = UI_Output(
        output_dir,
        mpi_comm,
        macsys,
        ["t", "u", "ue", "i", "i_ct_p", "i_ct_n"],
        disp=True,
    )
    problem = macsys.macroproblem
    problem.appendoutput(ui)
    if outputstate:
        outputstate = StateSpaceOutput(
            mpi_comm, macsys.statespace, output_dir, outputstate
        )
        problem.appendoutput(outputstate)

    problem.setupSolver()
    macsys.dumpparameter(output_dir)

    macsys.initialise()
    macsys.setCpbtoSol(1.0)
    problem.initialise()
    cap = macsys.cap

    problem.setTimeStep(1e-1)
    macsys.calculateSteadyState(True)
    problem.initialise()
    problem.output(0.0)

    print("Starting pulses")
    for I, tload, tpau in pulse:
        macsys.setTerminalCurrent(I)
        problem.setTimeStep(1e-3)
        problem.solve(problem.t + tload)
        macsys.setTerminalCurrent(0.0)
        problem.solve(problem.t + tpau)
