import math
import os
import pathlib
import sys

import helper.petscevent as event
import numpy as np
from helper.calcZ import calcImpedanceUsingFFT
from helper.ui_output import UI_Output
from helper.ui_output import UI_Store


def calculateImpedance(f, ui_eis, periods):
    t = np.array(ui_eis.t)
    i_ac = np.array(ui_eis.I)
    u = np.array(ui_eis.U)
    upos = np.array(ui_eis.Upos)
    uneg = np.array(ui_eis.Uneg)
    samples = 2 ** (math.ceil(math.log2(i_ac.size)) + 3)
    t = t - t[0]
    dudt = (u[-1] - u[0]) / (t[-1] - t[0])
    u = u - dudt * (t - t[0])
    t_new = np.linspace(t[0], t[-1], samples + 1)
    u = np.interp(t_new, t, u)
    i_ac = np.interp(t_new, t, i_ac)
    [ReZ, ImZ] = calcImpedanceUsingFFT(i_ac[0:-1], u[0:-1], periods, samples)

    dudt_pos = (upos[-1] - upos[0]) / (t[-1] - t[0])
    upos = upos - dudt_pos * (t - t[0])
    upos = np.interp(t_new, t, upos)
    [ReZp, ImZp] = calcImpedanceUsingFFT(i_ac[0:-1], upos[0:-1], periods, samples)

    dudt_neg = (uneg[-1] - uneg[0]) / (t[-1] - t[0])
    uneg = uneg - dudt_neg * (t - t[0])
    uneg = np.interp(t_new, t, uneg)
    [ReZn, ImZn] = calcImpedanceUsingFFT(i_ac[0:-1], uneg[0:-1], periods, samples)
    return ReZ, ImZ, ReZp, ImZp, ReZn, ImZn


def runEIS(macsys, problem, frequencies, I_dc, I_ac):
    mpi_comm = macsys.comm
    cap = macsys.cap
    pid = os.getpid()
    # print("Starting EIS at %d " %  pid)
    t0 = float(problem.t)
    tdc = max(0.0, 3600.0 * 1e-2 * cap / abs(I_dc))
    # print("Starting DC for %fs" % tdc)
    macsys.setTerminalCurrent(I_dc)
    problem.h = 1e-3
    problem.solve(t0 + tdc)
    n = 16.0
    k = 3
    dt = 1.0 / (frequencies[0] * n)
    timeisover = event.TimeEvent(dt=4 * tdc, t0=t0, terminate=True)
    Z = []

    for f, iac in zip(frequencies, I_ac):
        #   print("Starting EIS at %f Hz %f Adc %f Aac at %d" % (f, I_dc, iac, pid))
        eisoutput = UI_Store(macsys)
        t0 = float(problem.t)
        t = float(problem.t)
        omega = 2.0 * np.pi * f
        _acfunc = lambda x: I_dc + iac * np.math.sin(omega * (x - t0))
        eventhandler = event.EventHandler()
        eisevent = event.EisEvent(
            f,
            n,
            k,
            eisoutput=eisoutput,
            init=event.SetI(_acfunc, macsys),
            postevent=event.ConstI(I_dc, macsys),
        )
        eventhandler.add(eisevent)
        eventhandler.add(timeisover)
        problem.setEvent(eventhandler)
        problem.h = 1.0 / (f * n)
        problem.solve(eisevent.tend)
        Re, Im, Rep, Imp, Ren, Imn = calculateImpedance(f, eisoutput, int(k))
        Z.append((f, Re * cap, Im * cap, Rep * cap, Imp * cap, Ren * cap, Imn * cap))

    macsys.setTerminalCurrent(I_dc)
    t0 = float(problem.t)
    problem.solve(t0 + 300)
    return Z
