from helper.ui_output import UI_Output


def runinit(macsys):
    mpi_comm = dolfin.MPI.comm_world

    problem = macsys.macroproblem
    mpi_comm.barrier()
    problem.setupSolver()
    mpi_comm.barrier()
    ui = UI_Output(
        output_dir=None,
        comm=mpi_comm,
        macsys=macsys,
        header=["t", "u", "ue", "i", "i_ct_p", "i_ct_n"],
        disp=True,
    )
    problem = macsys.macroproblem
    problem.appendoutput(ui)

    macsys.initialise()
    macsys.setCpbtoSol(1.0)
    problem.initialise()
    cap = macsys.cap

    I20 = cap / 20
    mpi_comm.barrier()
    macsys.setTerminalCurrent(0)
    macsys.calculateSteadyState(True)
    problem.initialise()
    AM = macsys.NAM
    macsys.setTerminalCurrent(-I20)
    problem.solve(100)
    macsys.setTerminalVoltage(2.1)
    macsys.calculateSteadyState(True)
    problem.solve(0.2)
