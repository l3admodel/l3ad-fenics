import pathlib
import sys

import dolfin
import helper.petscevent as event
import multiphenics as mp
import numpy as np
from helper.parameteroutput import ParameterOutput
from helper.statespace import StateSpaceOutput
from helper.ui_output import UI_Output


def run(macsys, output_dir, U, I, outputstatedt=None):
    mpi_comm = dolfin.MPI.comm_world

    ui = UI_Output(
        output_dir,
        mpi_comm,
        macsys,
        ["t", "u", "ue", "i", "i_ct_p", "i_ct_n"],
        disp=True,
    )
    problem = macsys.macroproblem
    problem.appendoutput(ui)
    if outputstatedt:
        outputstate = StateSpaceOutput(
            mpi_comm, macsys.statespace, output_dir, outputstatedt
        )
        problem.appendoutput(outputstate)
        parameterout = ParameterOutput(macsys, output_dir, outputstatedt)
        problem.appendoutput(parameterout)
    problem.setupSolver()
    sys.stdout.flush()
    macsys.dumpparameter(output_dir)

    macsys.initialise()
    macsys.setCpbtoSol(1.0)
    problem.initialise()
    cap = macsys.cap

    uevent = event.UEvent(U, macsys, crossing=-1)
    eventhandler = event.EventHandler(tolerance=1e-4)
    eventhandler.add(uevent)
    problem.timeintegrator.setEventHandler(eventhandler)
    problem.setTimeStep(1e-3)
    macsys.calculateSteadyState(True)
    problem.initialise()
    problem.output(0.0)
    macsys.setTerminalCurrent(I)
    problem.solve(3600 * 300)
