import dolfin
from helper.modelblock import ModelBlock

from .structure import Structure


class ActiveMass(ModelBlock):
    def __init__(
        self, dx, meshinformation, statespace, elec_porosity, T, parameter, Id
    ):
        super().__init__(statespace, meshinformation)
        self._dx = dx
        self.parameter = parameter[self.name]
        self.Id = Id
        self.activemass = Structure(
            dx=dx,
            meshinformation=meshinformation,
            statespace=statespace,
            elec_porosity=elec_porosity,
            T=T,
            parameter=parameter,
            Id=Id,
            name=self.name,
        )
        self.reaction.dx = self.activemass.dx
        self.doublelayer.dx = self.activemass.dx

    def update(self, u):
        self.activemass.update(u)
        self.reaction.update(u)

    @property
    def us(self):
        return self._us

    @us.setter
    def us(self, us):
        self._us = us
        self.reaction.us = us
        self.sidereaction.us = us
        self.doublelayer.us = us

    @property
    def ueq(self):
        return self.reaction.ueq

    @property
    def dx(self):
        return self._dx

    @property
    def cap(self):
        return self.activemass.cap

    def A(self, u):
        return self.activemass.A(u)

    @property
    def V0active(self):
        return self.activemass.V0active

    @property
    def v_m(self):
        return self.activemass.v_m

    def getIct(self):
        return self.reaction.getIct(self.activemass.A(self.statespace.u))

    def jElectrolyteInterface(self, u, udot=None):
        g = self.activemass.A(u) * self.reaction.jElectrolyteInterface(u)
        g += self.activemass.A(u) * self.sidereaction.jElectrolyteInterface(u)
        if udot:
            nd = self.statespace.namedict
            g -= udot[nd["q"]]
        return g

    def jSolidInterface(self, u, udot=None):
        g = self.activemass.A(u) * self.reaction.jSolidInterface(u)
        g += self.activemass.A(u) * self.sidereaction.jSolidInterface(u)
        if udot:
            nd = self.statespace.namedict
            g += udot[nd["q"]]
        return g

    def getJ(self, u, state):
        return self.activemass.A(u) * self.reaction.getJ(u, state)

    def JDissolution(self, u):
        return self.activemass.A(u) * self.reaction.JDissolution(u)

    def output_parameter(self, t, outdir):
        pass

    def F(self, u, v, udot=None, u0=None):
        D = self.doublelayer.F(u=u, v=v, A=self.activemass.A(u), udot=udot, u0=u0)
        R = self.reaction.F(u=u, v=v, A=self.activemass.A(u), udot=udot, u0=u0)
        X = self.activemass.F(u=u, v=v, J=self.JDissolution(u), udot=udot, u0=u0)
        return [sum(f) for f in zip(D, R, X)]

    def initu(self, u):
        self.reaction.initu(u)
