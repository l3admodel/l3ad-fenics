import dolfin
import helper.constants as con
import numpy as np


class Conductivity:
    def __init__(self, parameter_charged, parameter_discharged):

        self.sigma1 = parameter_charged["material"]["sigma"]
        self.sigma2 = parameter_discharged["sigma"]
        v_m1 = parameter_charged["material"]["molar_volume"]
        v_m2 = parameter_discharged["molar_volume"]
        self.dc = parameter_charged["material"]["critical density"]
        self.vp = parameter_charged["material"]["inactive part"]
        self.C = (v_m2 - v_m1) / v_m1
        self.volume_relation = v_m2 / v_m1
        initial_porosity = parameter_charged["material"]["initial_porosity"]
        self.V0 = (
            1.0 - initial_porosity - parameter_charged["material"]["inactive part"]
        )

    def _calculate_k(self, porosity):
        return (1.0 - porosity) * 2.0 / self.dc

    def _calculateQ(self, X, k):
        V1 = X * self.V0
        V2 = (1 - X) * self.V0 * self.volume_relation + self.vp
        Vges = V1 + V2
        return (0.5 * k * V1 / Vges - 1.0) * self.sigma1 + (
            0.5 * k * V2 / Vges - 1.0
        ) * self.sigma2

    def calculate(self, X, p):
        k = self._calculate_k(p)
        Q = self._calculateQ(X, k)
        S = (
            (1 - p)
            / (k - 2.0)
            * (Q + np.sqrt(Q * Q + 2 * (k - 2.0) * self.sigma1 * self.sigma2))
        )
        return S
