import dolfin
import helper.constants as con
import numpy as np
import ufl
from helper.domainvalue import DomainAssigner

from .conductivity import Conductivity
from .porosity import Porosity


class Structure:
    def __init__(
        self,
        dx,
        meshinformation,
        statespace,
        elec_porosity,
        parameter,
        Id,
        name,
        **kwargs
    ):
        parameter_charged = parameter[name]
        parameter_electrolyte = parameter["electrolyte"]
        parameter_discharged = parameter["PbSO4"]
        self.comm = dolfin.MPI.comm_world
        self.statespace = statespace
        nd = self.statespace.namedict
        X = self.statespace.u[nd["X"]]
        self.Id = Id
        self.dx = dx

        self._setParameter(parameter_charged, parameter_electrolyte)
        self.sigma_am = Conductivity(parameter_charged, parameter_discharged)

        self.porosity_am = Porosity(
            X.function_space(),
            self.statespace.namedict,
            parameter_charged,
            parameter_discharged,
        )
        self.elec_porosity = elec_porosity
        one = dolfin.interpolate(dolfin.Constant(1), X.function_space())
        volume = dolfin.assemble(one * dx)
        self.v_m = parameter_charged["material"]["molar_volume"]
        initial_porosity = parameter_charged["material"]["initial_porosity"]
        self.V0 = (
            1.0 - initial_porosity - parameter_charged["material"]["inactive part"]
        )
        C = (
            volume
            * self.V0
            / (parameter_charged["material"]["molar_volume"])
            * 2.0
            * con.F
        )
        Ah = C / 3600
        self._cap = Ah * parameter_charged["utilisation"]
        self.cap_total = Ah
        mesh = meshinformation.mesh
        cell_domains = meshinformation.celldomain
        self.domain_assigner = DomainAssigner(
            cell_domains, X.function_space(), mesh, self.Id
        )
        self.comm.barrier()

    @property
    def cap(self):
        return self._cap

    @property
    def porosity(self):
        return self.porosity_am.value

    def update(self, u):
        self.porosity_am.update(u)
        self.domain_assigner.assign(self.elec_porosity, self.porosity)

    def updateSigma(self, Xarr, sigmaarray):
        S = self.sigma_am.calculate(
            Xarr[self.domain_assigner.dof],
            self.porosity.vector().get_local()[self.domain_assigner.dof],
        )
        sigmaarray[self.domain_assigner.dof] = S

    def _setParameter(self, parameter_charged, parameter_electrolyte):
        self.initial_porosity = parameter_charged["material"]["initial_porosity"]
        V0active = (
            1.0 - self.initial_porosity - parameter_charged["material"]["inactive part"]
        )
        A_initial = (
            V0active
            * parameter_charged["material"]["density"]
            * parameter_charged["material"]["aec_permass"]
        )
        self.A0 = A_initial
        self.X_factor = dolfin.Constant(
            parameter_charged["material"]["molar_volume"] / V0active
        )
        self.V0active = V0active

    def A(self, u):
        nd = self.statespace.namedict
        X = u[nd["X"]]
        return ufl.Max((self.A0 * X ** (2 / 3) - self.Acryst(u)), self.A0 * 5e-2)

    def FX(self, u, v, J, udot=None, u0=None):
        nd = self.statespace.namedict
        v = v[nd["X"]]
        if udot:
            Xdot = udot[nd["X"]]
            return (Xdot * dolfin.inv(self.X_factor) - J) * v * self.dx
        elif u0:
            X = u[nd["X"]]
            X0 = u0[nd["X"]]
            return (X - X0) * v * self.dx

    def F(self, u, v, J, udot, u0):
        nd = self.statespace.namedict
        f = [0] * len(u)
        f[nd["X"]] = self.FX(u, v, J, udot, u0)
        return f
