import dolfin
import helper.constants as con
import numpy as np
from helper.coefficient import Coefficient


class Porosity(Coefficient):
    def __init__(self, W, namedict, parameter_charged, parameter_discharged):
        super().__init__("AMporosity", W, namedict)
        self.initial_porosity = parameter_charged["material"]["initial_porosity"]
        molvolume_charged = parameter_charged["material"]["molar_volume"]
        molvolume_discharged = parameter_discharged["molar_volume"]
        self.inactive_part = parameter_charged["material"]["inactive part"]
        self.V0 = (
            1.0 - self.initial_porosity - parameter_charged["material"]["inactive part"]
        )

        self._factor = molvolume_discharged / molvolume_charged

    def update(self, u):
        nd = self.namedict
        x = u[nd["X"]]
        X = x.vector().get_local()
        self.k.vector().set_local(
            np.clip(
                1.0 - self.inactive_part - self.V0 * (X + self._factor * (1 - X)),
                0.0,
                1.0,
            )
        )
        self.k.vector().apply("insert")

    def Pdot(self, udot):
        nd = self.namedict
        Xdot = udot[nd["X"]]
        return -self.V0 * (1.0 - self._factor) * Xdot
