import dolfin
from helper.domainvalue import DomainAssigner
from helper.transport import Transport


class Electrode:
    def __init__(
        self, meshinformation, statespace, AM, parameter, id_grid, id_terminal, **kwargs
    ):
        self.AM = AM
        self.AM.us = self.us
        self.dx = AM.dx + meshinformation.dx(id_grid)
        self.ds = meshinformation.ds(id_terminal)
        self.meshinformation = meshinformation
        self.statespace = statespace
        self.id_grid = id_grid
        self.id_terminal = id_terminal
        self.parameter = parameter[self.name]

    def update(self, Xarray, sigmaarray):
        self.AM.activemass.updateSigma(Xarray, sigmaarray)

    def setSigma(self, sigma):
        DomainAssigner(
            self.meshinformation.celldomain,
            sigma.function_space(),
            self.meshinformation.mesh,
            self.id_grid,
        ).assign(sigma, self.parameter["sigma_grid"])

    def _S(self, u, udot, v):
        j = self.AM.jSolidInterface(u, udot)
        return j * v * self.AM.dx


class PositiveElectrode(Electrode):
    name = "PbO2"
    us = "us"

    def initu(self, u):
        nd = self.statespace.namedict
        us = u[nd[self.us]]
        ue = u[nd["ue"]]
        V = us.function_space()
        tmp = dolfin.project(self.AM.ueq + ue, V)
        DomainAssigner(
            self.meshinformation.celldomain,
            us.function_space(),
            self.meshinformation.mesh,
            self.AM.Id,
        ).assign(us, tmp)


class NegativeElectrode(Electrode):
    name = "Pb"
    us = "us"

    def initu(self, u):
        pass
