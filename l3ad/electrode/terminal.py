import numbers
import sys
import types

import dolfin
from helper.modelblock import ModelBlock


class Terminal(ModelBlock):
    def __init__(self, statespace, meshinformation, id_pos, id_neg, **kwargs):
        super().__init__(statespace, meshinformation)

        self.idpos = id_pos
        self.idneg = id_neg
        self.ds_pos = meshinformation.ds(id_pos)
        self.ds_neg = meshinformation.ds(id_neg)
        n = meshinformation.n
        self.A_pos = dolfin.assemble(1.0 * self.ds_pos)
        self.A_neg = dolfin.assemble(1.0 * self.ds_neg)

        self.k_terminal = dolfin.Constant(0.0)
        nd = self.statespace.namedict
        u = self.statespace.u
        l = u[nd["it"]]
        # up= u[nd["up"]]
        # un= u[nd["un"]]
        us = u[nd["us"]]
        self.set_terminal_potential = False

        self.u_form_pos = dolfin.Form(us * self.ds_pos)
        self.i_form_pos = dolfin.Form(l * self.ds_pos)
        self.i_form_neg = dolfin.Form(l * self.ds_neg)
        self.u_form_neg = dolfin.Form(us * self.ds_neg)
        self.iterminal_fac = 1.0 / self.A_pos

        if "i_set" in kwargs:
            self.setTerminalCurrent(kwargs["i_set"])
        elif "u_set" in kwargs:
            self.setTerminalVoltage(kwargs["u_set"])
        else:
            self.setTerminalCurrent(0.0)

    def sett(self, t):
        if self.set_terminal_potential == False:
            j = self._tcfunction(t) * self.iterminal_fac
            self.k_terminal.assign(j)
        else:
            u = self._tcfunction(t)
            self.k_terminal.assign(u)

    def F(self, u, v, udot=None, u0=None):
        nd = self.statespace.namedict
        f = [0] * len(u)
        f[nd["it"]] = self.Fl(u, v)
        return f

    def Fl(self, u, v):
        nd = self.statespace.namedict
        # up=u[nd["up"]]
        # un=u[nd["un"]]
        us = u[nd["us"]]
        l = u[nd["it"]]
        m = v[nd["it"]]
        g = self._Aupos(us, m)
        g += self._Auneg(us, m)
        g += self._Al(l, m)
        g += self._K(m)
        return g

    def setTerminalCurrent(self, I):
        self.set_terminal_potential = False
        if isinstance(I, numbers.Number):
            self._tcfunction = lambda t: float(I)
        elif isinstance(I, types.FunctionType):
            self._tcfunction = I
        else:
            raise RuntimeError("supply float or function")

    def setTerminalVoltage(self, U):
        self.set_terminal_potential = True
        if isinstance(U, numbers.Number):
            self._tcfunction = lambda t: float(U)
        elif isinstance(U, types.FunctionType):
            self._tcfunction = U
        else:
            raise RuntimeError("supply float or function")

    def getTerminalCurrent(self):
        return dolfin.assemble(self.i_form_pos)

    def getTerminalVoltage(self):
        return (
            dolfin.assemble(self.u_form_pos) / self.A_pos,
            dolfin.assemble(self.u_form_neg) / self.A_neg,
        )

    # non public

    def _Aupos(self, u, m):
        if self.set_terminal_potential == True:
            return u * m * self.ds_pos
        else:
            return 0

    def _Auneg(self, u, m):
        return u * m * self.ds_neg

    def _Al(self, l, m):
        if self.set_terminal_potential == False:
            return l * m * self.ds_pos
        else:
            return 0

    def _K(self, m):
        return -self.k_terminal * m * self.ds_pos
