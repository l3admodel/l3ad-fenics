from .hampsonreaction import HampsonReaction
from .sinlgechargetransfer import SinlgeChargeTransfer
from .twostepreactionwith2adsorption import TwoStepReactionWithTwoAdsorption
from .twostepreactionwithadsorption import TwoStepReactionWithAdsorption


def createReaction(reactiontype, statespace, Ueq, T, parameter, name):
    switcher = {
        "onestep": SinlgeChargeTransfer,
        "twostep": TwoStepReactionWithAdsorption,
        "twostep2ads": TwoStepReactionWithTwoAdsorption,
        "hampson": HampsonReaction,
    }
    return switcher[reactiontype](
        statespace=statespace, Ueq=Ueq, T=T, parameter=parameter, name=name
    )
