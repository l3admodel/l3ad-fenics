import electrolyte.sulfuricacid as sa
import helper.constants as con
import numpy as np


class EquilibirumPotential:
    def calculateEquilibirumPotential(self, C, T):
        m = sa.calculateMolality(C, T)
        x = np.log10(m)
        return (
            self.p1 * np.power(x, 4)
            + self.p2 * np.power(x, 3)
            + self.p3 * np.power(x, 2)
            + self.p4 * x
            + self.p5
        )


class PbO2EquilibirumPotential(EquilibirumPotential):
    p1 = 21.567e-3
    p2 = 43.220e-3
    p3 = 33.12e-3
    p4 = 73.924e-3
    p5 = 1.628194


class PbEquilibirumPotential(EquilibirumPotential):
    p1 = -0.012045
    p2 = -0.030552
    p3 = -0.030531
    p4 = -0.073585
    p5 = -0.29461
