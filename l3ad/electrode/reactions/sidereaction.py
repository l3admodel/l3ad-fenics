import dolfin
import helper.constants as con


class SideReaction:
    def __init__(self, statespace, T, parameter, name, **kwargs):
        self.parameter = parameter[name]
        self.name = name
        self._statespace = statespace
        self._ueq = dolfin.Constant(self.parameter["ueq"])
        self.T = T

    @property
    def statespace(self):
        return self._statespace

    def update(self, u):
        pass

    def jElectrolyteInterface(self, u):
        return -self.jSolidInterface(u)

    def jSolidInterface(self, u):
        return self.J(u)

    def JDissolution(self, u):
        return 0

    @property
    def ueq(self):
        return self._ueq

    def initu(self, u):
        pass

    def J(self, u):
        nd = self.statespace.namedict
        deltau = u[nd[self.us]] - u[nd["ue"]]
        du = deltau - self.ueq
        kF = dolfin.Constant(self.parameter["j0"])
        alpha = dolfin.Constant(self.parameter["alpha"])
        z = dolfin.Constant(self.parameter["z"])
        eta = z * dolfin.Constant(con.F / con.R) / self.T * du
        return kF * z * dolfin.exp(alpha * eta)
