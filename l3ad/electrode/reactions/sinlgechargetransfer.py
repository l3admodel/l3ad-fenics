import dolfin
import helper.constants as con
import ufl

from . import reaction


class SinlgeChargeTransfer(reaction.Reaction):
    reactiontype = "onestep"
    valence = 2

    def Jct(self, u):
        return self.ECReactionRate(u, "I")

    def JDissolution(self, u):
        return self.nu * self.Jct(u)
