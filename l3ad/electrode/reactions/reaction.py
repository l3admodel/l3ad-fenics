import dolfin
import helper.constants as con


class Reaction:
    def __init__(self, statespace, Ueq, T, parameter, name):
        self.parameter = parameter[name]
        self.name = name
        self.cascale = parameter["scale"]["cacid"]
        self.T = T
        self.nu = dolfin.Constant(parameter[name]["nu"])
        self.reactionUeq = Ueq
        self.statespace = statespace

    @property
    def statespace(self):
        return self._statespace

    def F(self, u, v, A, udot, u0):
        nd = self.statespace.namedict
        f = [0] * len(u)
        return f

    @statespace.setter
    def statespace(self, s):
        self._statespace = s
        nd = self._statespace.namedict
        cacid = self._statespace.u[nd["ca"]]
        self._ueq = dolfin.Function(cacid.function_space())
        self.c_acid = dolfin.Function(cacid.function_space())

    def update(self, u):
        nd = self.statespace.namedict
        cacid = u[nd["ca"]]
        self._calculateUeq(cacid)

    def _calculateUeq(self, cacid):
        new = self.reactionUeq.calculateEquilibirumPotential(
            self.cascale * cacid.vector().get_local(), float(self.T)
        )
        self._ueq.vector().set_local(new)
        self._ueq.vector().apply("insert")
        self.c_acid.vector().set_local(cacid.vector().get_local())
        self.c_acid.vector().apply("insert")

    @property
    def ueq(self):
        # return dolfin.Constant(0)
        return self._ueq

    def initu(self, u):
        pass

    def getIct(self, A):
        i = self.jSolidInterface(self.statespace.u)
        return dolfin.assemble(A * i * self.dx)

    def concentrationProduct(self, u, r=None):
        nd = self.statespace.namedict
        educts = 1
        products = 1
        sdict = self.getStoichiometry(r)
        for key, value in sdict.items():
            if key == "xi":
                x = self.xi(u)
            elif key == "e":
                x = 1
            else:
                x = u[nd[key]]
            if value > 0:
                if value == 1:
                    products *= x
                else:
                    products *= x ** value
            elif value < 0:
                if value == -1:
                    educts *= x
                else:
                    educts *= x ** abs(value)
        return (educts, products)

    def concentrationProductNormation(self, u=None, r=None):
        educts = 1
        products = 1
        sdict = self.getStoichiometry(r)
        for key, value in sdict.items():
            if value > 0:
                if value == 1:
                    products *= self.normed(key, u)
                else:
                    products *= self.normed(key, u) ** value
            elif value < 0:
                if value == -1:
                    educts *= self.normed(key, u)
                else:
                    educts *= self.normed(key, u) ** abs(value)
        return (educts, products)

    def normed(self, key, u):
        nd = self.statespace.namedict
        if key == "ca":
            # return u[nd['ca']]
            # return dolfin.Constant(4.)
            return self.c_acid
        elif key == "cpb":
            #    return u[nd['cpb']]
            # return dolfin.Constant(7.11)
            return self.solubilty_pb
        elif key == "e":
            return 1
        else:
            return None

    def getStoichiometry(self, r):
        return self.getReactionParameter(r)["stoichiometry"]

    def getReactionParameter(self, r=None):
        if r:
            return self.parameter["reactions"][r]
        else:
            return self.parameter["reactions"]

    def ECReactionRate(self, u, r=None):
        nd = self.statespace.namedict
        deltau = u[nd[self.us]] - u[nd["ue"]]
        parameter = self.getReactionParameter(r)

        n = parameter["n"]
        k = dolfin.Constant(parameter["k"])
        k.rename(f"k_{r:s}", f"k_{r:s}")
        alpha = dolfin.Constant(parameter["alpha"])
        alpha.rename(f"a_{r:s}", f"a_{r:s}")
        beta = 1 - alpha
        (educts, products) = self.concentrationProduct(u, r)
        (eductsN, productsN) = self.concentrationProductNormation(u, r)
        du = deltau - (
            self.ueq
            + con.R
            * self.T
            / (n * con.F)
            * dolfin.ln(products / educts * eductsN / productsN)
        )
        eta = n * con.F / con.R / self.T * du
        fp = dolfin.exp(alpha * eta)  # *educts*dolfin.inv(eductsN)
        fn = dolfin.exp(-beta * eta)  # *products* dolfin.inv(productsN)
        j0 = k * (educts ** beta) * (products ** alpha)
        return j0 * (fp - fn)

    def ReactionRate(self, u, r):
        parameter = self.getReactionParameter(r)
        if parameter["type"] == "EC":
            return self.ECReactionRate(u, r)
        elif parameter["type"] == "CHEM":
            return self.ChemReactionRate(u, r)
        else:
            raise RuntimeError("Invalid ReactionType")

    def ChemReactionRate(self, u, r=None):
        parameter = self.getReactionParameter(r)
        kf = dolfin.Constant(parameter["kf"])
        kb = dolfin.Constant(parameter["kb"])
        (educts, products) = self.concentrationProduct(u, r)
        return educts * kf - products * kb

    def jSolidInterface(self, u):
        j = 0
        for key, value in self.parameter["reactions"].items():
            reactionpar = value
            if reactionpar["type"] == "EC":
                n = dolfin.Constant(reactionpar["n"])
                j += n * self.ECReactionRate(u=u, r=key)
        return dolfin.Constant(con.F) * j

    def jElectrolyteInterface(self, u):
        return -self.jSolidInterface(u)

    def getJ(self, u, state):
        j = 0
        for key, value in self.parameter["reactions"].items():
            stoic = self.getStoichiometry(key)
            if state in stoic.keys():
                j += stoic[state] * self.ReactionRate(u, key)
        return j
