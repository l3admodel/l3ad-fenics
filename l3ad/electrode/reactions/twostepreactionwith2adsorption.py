import dolfin

from . import adsorptionreaction


class TwoStepReactionWithTwoAdsorption(adsorptionreaction.AdsorptionReaction):
    reactiontype = "twostep2ads"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.kf = dolfin.Constant(self.getReactionParameter("III")["kf"])
        self.kf.rename("kf_III", "kf_III")
        self.kb = dolfin.Constant(self.getReactionParameter("III")["kb"])
        self.kb.rename("kb_III", "kb_III")

    def theta10(self, u):
        nu = abs(self.getStoichiometry("III")["cpb"])
        if nu == 0:
            cpb0 = 1
        elif nu == 1:
            cpb0 = self.normed("cpb", u)
        kb = self.kb * cpb0
        kf = self.kf
        return kf * dolfin.inv(kb + 2 * kf)

    def theta20(self, u):
        nu = abs(self.getStoichiometry("III")["cpb"])
        if nu == 0:
            cpb0 = 1
        elif nu == 1:
            cpb0 = self.normed("cpb", u)
        kb = self.kb * cpb0
        kf = self.kf
        #       return (kb * (1 - self.theta10(u)) * dolfin.inv(kf + kb))
        return kb * dolfin.inv(kb + 2 * kf)
