import dolfin

from . import reaction


class AdsorptionReaction(reaction.Reaction):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    @property
    def Sigma0(self):
        return dolfin.Constant(self.parameter["adsorption"]["Sigma0"], name="Sigma0")

    def F(self, u, v, A, udot, u0):
        nd = self.statespace.namedict
        f = super().F(u, v, A, udot, u0)
        for key, value in self.parameter["adsorption"].items():
            if key.startswith("theta"):
                f[nd[key]] = self.Ftheta(u, v, key, udot, u0)
        return f

    def Ftheta(self, u, v, key, udot=None, u0=None):
        nd = self.statespace.namedict
        v = v[nd[key]]
        J = self.getJ(u, key)
        if udot:
            thetadot = udot[nd[key]]
            return (thetadot - J * dolfin.inv(self.Sigma0)) * v * self.dx
        elif u0:
            #            c = u[nd[key]]
            #            c0 = u0[nd[key]]
            #            return (c-c0) * v * self.dx
            return J * dolfin.inv(self.Sigma0) * v * self.dx

    def xi(self, u):
        nd = self.statespace.namedict
        a = 0
        ads_dict = self.parameter["adsorption"]
        for key, value in nd.items():
            if key.startswith("theta"):
                a += ads_dict[key] * u[value]
        return 1 - a

    def xi0(self, u):
        nd = self.statespace.namedict
        a = 0
        ads_dict = self.parameter["adsorption"]
        for key, value in nd.items():
            if key.startswith("theta"):
                u0 = getattr(self, "%s0" % key)(u)
                a += ads_dict[key] * u0
        return 1 - a

    def normed(self, key, u):
        ads_dict = self.parameter["adsorption"]
        if key in ads_dict.keys() and key.startswith("theta"):
            return getattr(self, "%s0" % key)(u)
        elif key == "xi":
            return self.xi0(u)
        else:
            return super().normed(key, u)

    def initu(self, u):
        nd = self.statespace.namedict
        ads_dict = self.parameter["adsorption"]
        for key, value in nd.items():
            if key.startswith("theta"):
                u0 = getattr(self, "%s0" % key)(u)
                tmp = dolfin.project(u0, u[nd[key]].function_space())
                dolfin.assign(u[nd[key]], tmp)

    def JctI(self, u):
        return self.ECReactionRate(u, "I")

    def JDissolution(self, u):
        return self.nu * self.JctI(u)
