import dolfin

from . import adsorptionreaction


class HampsonReaction(adsorptionreaction.AdsorptionReaction):
    reactiontype = "hampson"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.kf = dolfin.Constant(self.getReactionParameter("II")["kf"])
        self.kb = dolfin.Constant(self.getReactionParameter("II")["kb"])
        self.kf.rename("kf_II", "kf_II")
        self.kb.rename("kb_II", "kb_II")

    def theta10(self, u):
        nu = abs(self.getStoichiometry("II")["ca"])
        if nu == 0:
            ca = 1
        elif nu == 1:
            ca = self.normed("ca", u)
        kf = self.kf
        kb = self.kb * ca
        return (dolfin.sqrt(kb ** 2 + 2 * kb * kf) - kb) * dolfin.inv(2 * kf)

    def theta20(self, u):
        nu = abs(self.getStoichiometry("II")["ca"])
        if nu == 0:
            ca = 1
        elif nu == 1:
            ca = self.normed("ca", u)
        kf = self.kf
        kb = self.kb * ca
        #        return (kb -dolfin.sqrt(kb**2+2*kf*kb)+kf)*dolfin.inv(2*kf)
        return (kf * self.theta10(u) ** 2) * dolfin.inv(kb)
