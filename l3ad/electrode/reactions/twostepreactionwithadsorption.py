import dolfin

from . import adsorptionreaction


class TwoStepReactionWithAdsorption(adsorptionreaction.AdsorptionReaction):
    reactiontype = "twostep"

    def theta0(self, u):
        return dolfin.Constant(0.5)
