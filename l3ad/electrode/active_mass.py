import dolfin
from electrode.reactions import equilibirumpotential as eqp

from .activemass.activemass import ActiveMass
from .doublelayer import DoubleLayer
from .reactions.reactioncreator import createReaction
from .reactions.sidereaction import SideReaction
from .reactions.sinlgechargetransfer import SinlgeChargeTransfer


class PbO2ActiveMass(ActiveMass):
    reactionUeq = eqp.PbO2EquilibirumPotential()
    name = "PbO2"

    def __init__(
        self, dx, meshinformation, statespace, elec_porosity, T, parameter, Id
    ):

        self.reaction = createReaction(
            parameter[self.name]["type"],
            statespace=statespace,
            Ueq=self.reactionUeq,
            T=T,
            parameter=parameter,
            name=self.name,
        )
        self.sidereaction = SideReaction(
            statespace=statespace, T=T, parameter=parameter, name="O2"
        )
        self.doublelayer = DoubleLayer(
            statespace=statespace, parameter=parameter[self.name]
        )
        super().__init__(
            dx=dx,
            meshinformation=meshinformation,
            statespace=statespace,
            elec_porosity=elec_porosity,
            T=T,
            parameter=parameter,
            Id=Id,
        )


class PbActiveMass(ActiveMass):
    reactionUeq = eqp.PbEquilibirumPotential()
    name = "Pb"

    def __init__(
        self, dx, meshinformation, statespace, elec_porosity, T, parameter, Id
    ):
        self.reaction = createReaction(
            parameter[self.name]["type"],
            statespace=statespace,
            Ueq=self.reactionUeq,
            T=T,
            parameter=parameter,
            name=self.name,
        )
        self.sidereaction = SideReaction(
            statespace=statespace, T=T, parameter=parameter, name="H2"
        )
        self.doublelayer = DoubleLayer(
            statespace=statespace, parameter=parameter[self.name]
        )
        super().__init__(
            dx=dx,
            meshinformation=meshinformation,
            statespace=statespace,
            elec_porosity=elec_porosity,
            T=T,
            parameter=parameter,
            Id=Id,
        )

    def printAEC(self):
        print(dolfin.assemble(self.activemass.A(self.statespace.u) * self.dx))
