import dolfin


class DoubleLayer:
    def __init__(self, statespace, parameter):
        self.parameter = parameter
        self.statespace = statespace

    @property
    def cdl(self):
        return dolfin.Constant(self.parameter["cdl"])

    @property
    def f(self):
        if "adsorption" in self.parameter.keys():
            return dolfin.Constant(self.parameter["adsorption"].get("f", 1))
        return 0

    def F(self, u, v, A, udot, u0):
        nd = self.statespace.namedict
        f = [0] * len(u)
        f[nd["q"]] = self._Fq(u, v, A)
        return f

    def _Fq(self, u, v, A):
        nd = self.statespace.namedict
        us = u[nd[self.us]]
        ue = u[nd["ue"]]
        du = us - ue
        q = u[nd["q"]]
        v = v[nd["q"]]
        if "adsorption" in self.parameter.keys():
            a = 0
            ads_dict = self.parameter["adsorption"]
            for key, value in nd.items():
                if key.startswith("theta"):
                    a += dolfin.Constant(ads_dict.get(key, 1)) * u[value]
            return (q - A * (1 + a * (self.f - 1)) * self.cdl * du) * v * self.dx
        return (q - A * self.cdl * du) * v * self.dx
