import os

import dolfin
from helper.transport import Transport


class ElectronTransport(Transport):
    def __init__(self, meshinformation, statespace, electrodep, electroden):

        self.electrodep = electrodep
        self.electroden = electroden
        ds = electroden.ds + electrodep.ds
        dx = electroden.dx + electrodep.dx
        super().__init__(
            meshinformation, statespace, dx, ds, meshinformation.electrodeshell, "us"
        )
        nd = self.statespace.namedict
        X = self.statespace.u[nd["X"]]
        self.sigma = dolfin.Function(X.function_space())
        self.electrodep.setSigma(self.sigma)
        self.electroden.setSigma(self.sigma)
        self.sigma_file = "parameter/sigma.xdmf"

    def output_parameter(self, t, outdir):
        comm = self.meshinformation.mesh.mpi_comm()
        with dolfin.XDMFFile(comm, os.path.join(outdir, self.sigma_file)) as f:
            f.parameters["rewrite_function_mesh"] = False
            f.write_checkpoint(self.sigma, "sigma", t, append=True)

    @property
    def phi(self):
        # return dolfin.Constant(4.55e6)
        return self.sigma

    def update(self, u):
        self.updateSigma(u)

    def updateSigma(self, u):
        nd = self.statespace.namedict
        X = u[nd["X"]]
        sigmaarray = self.sigma.vector().get_local()
        Xarr = X.vector().get_local()
        self.electrodep.update(Xarr, sigmaarray)
        self.electroden.update(Xarr, sigmaarray)
        self.sigma.vector().set_local(sigmaarray)
        self.sigma.vector().apply("insert")

    def _BC(self, u, v):
        nd = self.statespace.namedict
        it = u[nd["it"]]
        return self.poisson.KNeumannBC(v, it)

    def _S(self, u, udot, v):
        g = self.electrodep._S(u, udot, v)
        g += self.electroden._S(u, udot, v)
        return g

    def initu(self, u):
        self.electrodep.initu(u)
        self.electroden.initu(u)
