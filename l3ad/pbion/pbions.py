import dolfin
import electrolyte.sulfuricacid as sa
import helper.constants as con
import numpy as np
from helper.coefficient import Coefficient


class LeadSulfateDiffusionCoefficient(Coefficient):
    def __init__(self, W, T, namedict, parameter):
        super().__init__("Dpbso4", W, namedict)
        self.cascale = parameter["scale"]["cacid"]
        self.T = T

    def update(self, u):
        nd = self.namedict
        ca = u[nd["ca"]]

        cproject = dolfin.project(ca, self.k.function_space())
        self.k.vector().set_local(
            calculateDiffusionCoefficient(
                self.cascale * cproject.vector().get_local(), self.T
            )
        )
        self.k.vector().apply("insert")


def calculateDiffusionCoefficient(C, T):
    x = np.clip(C, 100, 5500)
    #    x = np.log(_C)
    #    p1 = -0.04086
    #    p2 = 0.6428
    #    p3 = -3.353
    #    p4 = -14.88
    p1 = 2.849e-11
    p2 = -2.685e-07
    p3 = 0.0003716
    p4 = -20.94
    return np.exp(p1 * np.power(x, 3) + p2 * np.power(x, 2) + p3 * x + p4) * T / 293.15


class LeadSulfateSolubility(Coefficient):
    def __init__(self, W, T, namedict, parameter):
        super().__init__("spbso4", W, namedict)
        self.cpbscale = parameter["scale"]["cpb"]
        self.cascale = parameter["scale"]["cacid"]
        self.T = T

    def update(self, u):
        nd = self.namedict
        ca = u[nd["ca"]]

        cproject = dolfin.project(ca, self.k.function_space())
        self.k.vector().set_local(
            calculateSolubilty(self.cascale * cproject.vector().get_local(), self.T)
            / self.cpbscale
        )
        self.k.vector().apply("insert")


def calculateSolubilty(C, T):
    c = np.clip(C, 100, 5500)
    E_a = 2798.0
    T0 = 293.15
    p1 = -4.637
    # -5.868;
    p2 = 3.049e4
    # 2.091e4;
    p3 = -7.089e7
    # -4.69e7 ;
    q1 = -7218
    # -3866;
    q2 = 1.74e7
    # 1.074e7;
    c2 = np.power(c, 2)
    nom = p1 * c2 + p2 * c + p3
    denom = c2 + q1 * c + q2
    return np.exp(np.divide(nom, denom)) * np.exp(-E_a / T + E_a / T0)
