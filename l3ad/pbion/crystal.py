import os

import dolfin
import numpy as np
import ufl
from helper.domainvalue import DomainAssigner
from helper.initstate import initstate
from pbion.nucleation import NucleationRate

Nmin = 1e2
amin = 2.0


class Crystal:
    def __init__(
        self, AM, statespace, solubilty, D, am_parameter, sulfate_parameter, scale, T
    ):
        self.AM = AM
        meshinformation = AM.meshinformation
        self.comm = meshinformation.mesh.mpi_comm()
        self.statespace = statespace
        dx = AM.dx
        self.volume = dolfin.assemble(1.0 * dx)
        self.T = T
        self.solubilty = solubilty
        self._D = D
        self.J = NucleationRate(T, am_parameter, sulfate_parameter)
        self.a0 = self.J.a0
        self.Nscale = dolfin.Constant(scale["N"])
        self.cpbscale = dolfin.Constant(scale["cpb"])
        self.amin = dolfin.Constant(amin)
        self.Nmin = dolfin.Constant(Nmin / self.Nscale)
        self.cmin = dolfin.Constant(1e-18 / self.cpbscale)
        self.v_m = dolfin.Constant(sulfate_parameter["molar_volume"])

        self.AM.activemass.Acryst = self.Acryst
        self.AM.reaction.solubilty_pb = self.solubilty
        c = statespace.u[statespace.namedict["cpb"]]
        a = statespace.u[statespace.namedict["r"]]
        n = statespace.u[statespace.namedict["n"]]
        self.Sform = dolfin.Form(c / self.solubilty * dx)
        self.rform = dolfin.Form(self.Nscale * a * n * dx)
        self.nform = dolfin.Form(self.Nscale * n * dx)

    def initr(self, u, u0, coors):
        Vr = u.function_space()
        r0 = initstate(Vr, u0, coors)
        r = dolfin.project(ufl.Max(r0, self.amin), Vr)
        meshinformation = self.AM.meshinformation
        DomainAssigner(
            meshinformation.celldomain, Vr, meshinformation.mesh, self.AM.Id
        ).assign(u, r)

    def initn(self, u, u0, coors):
        Vn = u.function_space()
        n0 = initstate(Vn, u0, coors)
        n = dolfin.project(ufl.Max(n0, self.Nmin), Vn)
        meshinformation = self.AM.meshinformation
        DomainAssigner(
            meshinformation.celldomain, Vn, meshinformation.mesh, self.AM.Id
        ).assign(u, n)

    def calculater(self, u):
        nd = self.statespace.namedict
        r = u[nd["r"]]
        N = u[nd["n"]]
        X = u[nd["X"]]
        Vr = r.function_space()
        vrel = self.AM.V0active * self.v_m / self.AM.v_m
        vfac = self.Vfactor
        r0 = dolfin.project(
            ufl.Max(
                ufl.Max((1.0 - X) * vrel, 0) ** (1 / 3)
                / ufl.Max(vfac * N, 1e-6) ** (1 / 3),
                self.amin,
            ),
            Vr,
        )
        meshinformation = self.AM.meshinformation
        DomainAssigner(
            meshinformation.celldomain, Vr, meshinformation.mesh, self.AM.Id
        ).assign(r, r0)

    def calculaten(self, u):
        nd = self.statespace.namedict
        r = u[nd["r"]]
        n = u[nd["n"]]
        X = u[nd["X"]]
        Vn = n.function_space()
        Vcrystal = dolfin.project(self.Vfactor * r ** 3, Vn)
        Vsulfate = dolfin.project(
            (1.0 - X) * dolfin.Constant(self.AM.V0active * self.v_m / self.AM.v_m), Vn
        )
        n0 = dolfin.project(ufl.Max(Vsulfate * dolfin.inv(Vcrystal), self.Nmin), Vn)
        meshinformation = self.AM.meshinformation
        DomainAssigner(
            meshinformation.celldomain, Vn, meshinformation.mesh, self.AM.Id
        ).assign(n, n0)

    @property
    def Savg(self):
        return dolfin.assemble(self.Sform) / self.volume

    @property
    def ravg(self):
        return dolfin.assemble(self.rform) / self.volume / self.navg

    @property
    def navg(self):
        return dolfin.assemble(self.nform) / self.volume

    @property
    def D(self):
        return self._D

    def Acryst(self, u):
        nd = self.statespace.namedict
        r = u[nd["r"]]
        n = u[nd["n"]]
        return self.Afactor * r ** 2 * n

    @property
    def Vfactor(self):
        return dolfin.Constant(self.J.V_factor * self.Nscale * self.a0 ** 3)

    @property
    def Afactor(self):
        return dolfin.Constant(self.J.A_factor * self.Nscale * self.a0 ** 2)

    def Mr(self, rdot, v):
        return rdot * v * self.AM.dx

    def Fr(self, u, v):
        nd = self.statespace.namedict
        a = ufl.Max(u[nd["r"]], self.amin)
        c = ufl.Max(u[nd["cpb"]], self.cmin)
        N = ufl.Max(u[nd["n"]], self.Nmin)
        vnuc = self._vnuc(a, N, c)
        vgrowth = self.J.adotfactor * self._vgrowth(a, c)
        vcoarse = self.J.adotfactor * self._fcoars(a, c) * self._vcoarse(c, a)
        adot = vgrowth + vcoarse + vnuc
        return (
            -ufl.conditional(
                ufl.gt(a, self.amin), adot, ufl.Max(adot, dolfin.Constant(0.0))
            )
            * v
            * self.AM.dx(metadata={"quadrature_degree": 5})
        )

    def MN(self, Ndot, v):
        return Ndot * v * self.AM.dx

    def FN(self, u, v):
        nd = self.statespace.namedict
        a = ufl.Max(u[nd["r"]], self.amin)
        c = ufl.Max(u[nd["cpb"]], self.cmin)
        N = ufl.Max(u[nd["n"]], self.Nmin)
        Jnuc = self.AM.A(u) * self._J(c) * c * self.cpbscale
        Ncoars = self._Ndotcoarse(a, c, N)
        ndot = ufl.conditional(ufl.gt(-Ncoars, Jnuc), self._fcoars(a, c) * Ncoars, Jnuc)

        Ndiss = self._Ndotdiss(a, c, N)
        Ndot = ndot + Ndiss
        return (
            -ufl.conditional(
                ufl.gt(N, self.Nmin), Ndot, ufl.Max(Ndot, dolfin.Constant(0.0))
            )
            * v
            * self.AM.dx(metadata={"quadrature_degree": 5})
        )

    def Fc(self, v, u, udot):
        nd = self.statespace.namedict
        a = ufl.Max(u[nd["r"]], self.amin)
        N = ufl.Max(u[nd["n"]], self.Nmin)
        adot = udot[nd["r"]]
        Ndot = udot[nd["n"]]
        Vfactor = dolfin.Constant(self.J.V_factor) * self.Nscale * self.a0 ** 3
        kcdot = Vfactor / self.v_m * dolfin.inv(self.cpbscale)
        fadot = 3.0 * N * pow(a, 2) * adot
        fndot = pow(a, 3) * Ndot
        return kcdot * (fadot + fndot) * v * self.AM.dx

    @property
    def vprefac(self):
        return (
            self.D
            * self.v_m
            * dolfin.inv(self.a0 ** 2)
            * self.cpbscale
            * self.J.adotfactor
        )

    def _cs(self, a):
        return self.solubilty * dolfin.exp(dolfin.inv(a))

    def _vcoarse(self, c, a):
        cs = self._cs(a)
        return ufl.conditional(
            ufl.gt(c, self.solubilty),
            4.0 / 27.0 * self.vprefac * cs * dolfin.inv(a ** 2),
            dolfin.Constant(0.0),
        )

    def _vgrowth(self, a, c):
        deltac = c - self._cs(a)
        f = self._fdis(a, c) * (1.0 - self._fcoars(a, c))
        return f * self.vprefac * deltac * dolfin.inv(a)

    def _vnuc(self, a, N, c):
        alpha = 1.1
        f = 1.0 - self._fcoars(a, c)
        return ufl.conditional(
            ufl.gt(c, self.solubilty),
            self._J(c)
            * dolfin.inv(ufl.Max(N, self.Nmin))
            * (alpha * ufl.Max(self._acrit(c), self.amin) - a),
            dolfin.Constant(0.0),
        )

    def _acrit(self, c):
        return ufl.conditional(
            ufl.gt(c, self.solubilty),
            dolfin.inv(dolfin.ln(c / self.solubilty)),
            dolfin.Constant(1e12),
        )

    def _fdis(self, a, c):
        x = a / self.amin
        # 1. means no dissolution
        return ufl.conditional(
            ufl.lt(c, self.solubilty),
            0.5 * (1.0 + ufl.tanh(x - 5)),
            dolfin.Constant(1.0),
        )

    def _fcoars(self, a, c):
        acrit = self._acrit(c)
        k = dolfin.Constant(1000)
        x = a / acrit - 1.0
        flow = k * x ** 2
        fhigh = dolfin.erf(2 * x)
        f = ufl.conditional(ufl.gt(a, acrit), fhigh, flow)
        g = ufl.conditional(ufl.gt(c, self.solubilty), f, dolfin.Constant(1.0))
        return ufl.Max(1.0 - g, dolfin.Constant(0.0))

    def _Ndotcoarse(self, a, c, N):
        cs = self._cs(a) * self.cpbscale
        v = self._vcoarse(c, a) * dolfin.inv(a)
        return v * (
            cs * self.v_m * dolfin.inv(a) * (dolfin.inv(self.Vfactor * pow(a, 3)) - N)
            - 3 * N
        )

    def _Ndotdiss(self, _a, c, N):
        a = ufl.Max(_a, self.amin)
        f = 1.0 - self._fdis(a, c)
        return ufl.Min(
            f * 3.0 * self._vgrowth(a, c) * dolfin.inv(a) * N, dolfin.Constant(0.0)
        )

    def _J(self, c):
        return (
            dolfin.inv(self.Nscale)
            * c
            * self.cpbscale
            * self.J(c, self.solubilty, self._D)
        )
