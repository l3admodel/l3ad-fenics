import dolfin
import numpy as np
import pbion.pbions as pbions
from helper.domainvalue import DomainAssigner
from helper.masstransport import MassTransport
from pbion.crystal import Crystal


class PbIonConcentration(MassTransport):
    def __init__(self, meshinformation, statespace, PAM, NAM, porosity, T, parameter):
        super().__init__(
            meshinformation=meshinformation,
            statespace=statespace,
            PAM=PAM,
            NAM=NAM,
            porosity=porosity,
            T=T,
            shell=meshinformation.electrolyteshell,
            statename="cpb",
            alpha=parameter["PbSO4"]["alpha"],
        )

        sulfate_parameter = parameter["PbSO4"]
        self.factor = dolfin.Constant(sulfate_parameter["factor"])
        self.cpbscale = parameter["scale"]["cpb"]
        self.cascale = parameter["scale"]["cacid"]

        W0 = statespace.u[statespace.namedict["cpb"]].function_space()
        self.spbso4 = pbions.LeadSulfateSolubility(
            W0, T, statespace.namedict, parameter
        )
        self.Dpbso4 = pbions.LeadSulfateDiffusionCoefficient(
            W0, T, statespace.namedict, parameter
        )
        self.porosity = porosity
        self.crystalPAM = Crystal(
            self.PAM,
            statespace,
            self.solubility,
            self.Dpbso4.value,
            PAM.parameter,
            sulfate_parameter,
            parameter["scale"],
            T,
        )
        self.crystalNAM = Crystal(
            self.NAM,
            statespace,
            self.solubility,
            self.Dpbso4.value,
            NAM.parameter,
            sulfate_parameter,
            parameter["scale"],
            T,
        )

    def output_parameter(self, *args):
        pass

    @property
    def D(self):
        return self.factor * self.Dpbso4.value * self.porosity

    @property
    def solubility(self):
        return self.spbso4.value

    def initcPbIon(self, c0, c):
        meshinformation = self.meshinformation
        id_dict = meshinformation.id_dict
        ids = [self.PAM.Id, self.NAM.Id, id_dict["electrolyte"]]
        if isinstance(c0, float):
            tmp = dolfin.project(dolfin.Constant(c0), c.function_space())
        else:
            tmp = dolfin.project(c0, c.function_space())
        DomainAssigner(
            meshinformation.celldomain, c.function_space(), meshinformation.mesh, ids
        ).assign(c, tmp)

    def setCtoSol(self, cpb, f):
        meshinformation = self.meshinformation
        id_dict = meshinformation.id_dict
        ids = [self.PAM.Id, self.NAM.Id, id_dict["electrolyte"]]
        tmp = dolfin.project(f * self.solubility, cpb.function_space())
        DomainAssigner(
            meshinformation.celldomain, cpb.function_space(), meshinformation.mesh, ids
        ).assign(cpb, tmp)

    def update(self, u):
        self.spbso4.update(u)
        self.Dpbso4.update(u)

    def Fr(self, u, v, udot=None, u0=None):
        nd = self.statespace.namedict
        v = v[nd["r"]]
        if udot:
            rdot = udot[nd["r"]]
            g = self.crystalPAM.Mr(rdot, v)
            g += self.crystalNAM.Mr(rdot, v)
            g += self.crystalPAM.Fr(u, v)
            g += self.crystalNAM.Fr(u, v)
            return g
        elif u0:
            r0 = u0[nd["r"]]
            r = u[nd["r"]]
            g = self.crystalPAM.Mr(r - r0, v)
            g += self.crystalNAM.Mr(r - r0, v)
            return g

    def FN(self, u, v, udot=None, u0=None):
        nd = self.statespace.namedict
        v = v[nd["n"]]
        if udot:
            ndot = udot[nd["n"]]
            g = self.crystalPAM.MN(ndot, v)
            g += self.crystalNAM.MN(ndot, v)
            g += self.crystalPAM.FN(u, v)
            g += self.crystalNAM.FN(u, v)
            return g
        elif u0:
            n = u[nd["n"]]
            n0 = u0[nd["n"]]
            g = self.crystalPAM.MN(n - n0, v)
            g += self.crystalNAM.MN(n - n0, v)
            return g

    def _S(self, u, udot, v):
        Jp = self.PAM.getJ(u, "cpb")
        Jn = self.NAM.getJ(u, "cpb")
        scale = dolfin.Constant(self.cpbscale)
        g = super()._S(u, udot, v)
        g += -dolfin.inv(scale) * Jp * v * self.PAM.dx
        g += -dolfin.inv(scale) * Jn * v * self.NAM.dx
        g += self.crystalPAM.Fc(v, u, udot)
        g += self.crystalNAM.Fc(v, u, udot)
        return g

    def F(self, u, v, udot=None, u0=None):
        nd = self.statespace.namedict
        f = super().F(u=u, v=v, udot=udot, u0=u0)
        f[nd["r"]] = self.Fr(u=u, v=v, udot=udot, u0=u0)
        f[nd["n"]] = self.FN(u=u, v=v, udot=udot, u0=u0)
        return f
