import dolfin
import helper.constants as con
import numpy as np
import ufl


class NucleationRate:
    def __init__(self, T, parameter_am, parameter_sulfate, **kwargs):
        self.T = T
        v_m_am = parameter_am["material"]["molar_volume"]
        v_m_sulfate = parameter_sulfate["molar_volume"]
        initial_porosity = parameter_am["material"]["initial_porosity"]
        sigma_contact = (
            -parameter_am["crystal"]["sigma_acid"]
            + parameter_am["crystal"]["sigma_sulfate"]
        )
        sigma_interface = parameter_sulfate["crystal"]["sigma_acid"]
        self.v_a = v_m_sulfate / con.N
        v_a_am = v_m_am / con.N
        r_p = np.cbrt(0.75 / np.pi * v_a_am)
        C0 = parameter_am["crystal"]["seed_density"] / (r_p ** 2 * np.pi)

        fA_C = parameter_am["crystal"]["contactfactor"]
        fA_I = parameter_am["crystal"]["interfacefactor"]
        fV = parameter_am["crystal"]["volumefactor"]
        fV_S = parameter_am["crystal"]["volumefactor_reference"]
        self.V_factor = fV
        self.A_factor = fA_C
        self.C0 = dolfin.Constant(C0)

        A_C = fA_C * (self.v_a / fV) ** (2 / 3)
        A_I = fA_I * (self.v_a / fV) ** (2 / 3)
        sigma_eff = sigma_interface + fA_C / fA_I * sigma_contact
        self._nf = dolfin.Constant(8.0 / 27.0 * (A_I * sigma_eff) ** 3)
        self._Zf = dolfin.Constant(
            np.sqrt(9.0 / (16.0 * (A_I * sigma_eff) ** 3 * con.k_B * np.pi))
        )

        self._ffcrit = dolfin.Constant(
            np.power(48.0 * np.pi ** 2 * self.v_a, 1.0 / 3.0)  # fcrit factor
            * con.N  # conc zu #/m³
            * 2.0
            / 3.0
            * A_I
            * sigma_eff  # nf^1/3
            * fA_I
            / (4 * np.pi)  # Korrektur Oberfläche
        )
        self._a0 = dolfin.Constant(
            2.0 * fA_I / (fV * 3.0) * sigma_eff * v_m_sulfate / (con.R * T)
        )
        self.adotfactor = dolfin.Constant(fA_I / (3 * fV) * (fV_S / fV) ** (1 / 3))
        # print(float(self.a0), float(self.adotfactor))

    def __call__(self, c, cs, D):
        return ufl.conditional(
            ufl.gt(c, dolfin.Constant(2.0) * cs),
            self.C0
            * self._Z(c, cs)
            * self._fcrit(c, cs, D)
            * dolfin.exp(-self._Wcrit(c, cs) / (con.k_B * self.T)),
            dolfin.Constant(0.0),
        )

    @property
    def a0(self):
        return self._a0

    def delta_mu(self, c, cs):  # cpbscaleinvariant
        return con.k_B * self.T * dolfin.ln(c / cs)

    def n(self, c, cs):  # cpbscaleinvariant
        return self._nf * dolfin.inv(pow(self.delta_mu(c, cs), 3))

    def _Z(self, c, cs):  # cpbscaleinvariant
        return pow(self.delta_mu(c, cs), 2) * self._Zf * dolfin.inv(dolfin.sqrt(self.T))

    def _Wcrit(self, c, cs):  # cpbscaleinvariant
        Wf = 0.5 * self._nf
        return Wf * dolfin.inv(pow(self.delta_mu(c, cs), 2))

    def _fcrit(self, c, cs, D):
        return self._ffcrit * dolfin.inv(self.delta_mu(c, cs)) * D
