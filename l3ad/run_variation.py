import csv
import os
import pathlib
import shutil
import sys

import dolfin
import multiphenics as mp

# dolfin.set_log_level(10)

dolfin.parameters["ghost_mode"] = "shared_facet"
dolfin.parameters["form_compiler"]["optimize"] = True
dolfin.parameters["form_compiler"]["cpp_optimize_flags"] = "-O2"
dolfin.parameters["linear_algebra_backend"] = "PETSc"

import petsc4py

petsc4py.init(sys.argv)

import numpy as np
from helper.parameter import parameter
from helper.tolerance import getTolerance
from helper.bounds import getBounds
from simdata.createTestSystem import createSystem
from simdata.runVariationEIS import runvariationeis

# from simdata.runeis import run
# from simdata.runinit import runinit
from scipy.optimize import minimize
import simdata.measured as meas

from pbion.crystal import amin, Nmin
import multiprocessing
from itertools import product, chain

import helper.constants as con

mpi_comm = dolfin.MPI.comm_world

Nscale = parameter["scale"]["N"]
cascale = parameter["scale"]["cacid"]
cpbscale = parameter["scale"]["cpb"]
tolerance = getTolerance(parameter)
bounds = getBounds(parameter)
mpi_comm.barrier()

initstates = {
    "cacid": {"u0": 4200.0 / cascale},
    "X": {
        "+": {
            "u0": 0.9,
        },
        "-": {
            "u0": 0.9,
        },
    },
    "r": {
        "+": {
            "u0": 100,
        },
        "-": {
            "u0": 100,
        },
    }
    #    'n': {
    #        '+': {
    #            'u0': 6e17 / Nscale
    #        },
    #        '-': {
    #            'u0': 5e17 / Nscale
    #        }
    #    }
}


def runVar(pars):
    print("Parameter:", pars)
    a1, a2, S, k1, k2, k3, k4 = pars
    with open("{}/{}.csv".format("../output/var", "opt"), "a") as f:
        _csv = csv.writer(f, delimiter="\t")
        _csv.writerow([a1, a2, S, k1, k2, k3, k4])
    parameter["Pb"]["hampson"]["k"] = [k1, k2]
    parameter["Pb"]["hampson"]["kchem"] = [k3, k4]
    parameter["Pb"]["hampson"]["alpha"] = [a1, a2]
    parameter["Pb"]["Sigma0"] = S
    mpi_comm.barrier()
    mesh_dir = "../SmallCellh"
    mesh_name = "CellCoarse"
    cap = 2.0 * 0.5 / 36.0
    istate = initstates.copy()
    macsys = createSystem(
        mesh_dir, mesh_name, parameter, tolerance, bounds, istate, cap
    )
    try:
        Z = runvariationeis(macsys)
        f = []
        ReZ = []
        ImZ = []
        ReZp = []
        ImZp = []
        ReZn = []
        ImZn = []

        for z in Z:
            f.append(z[0])
            ReZ.append(z[1])
            ImZ.append(z[2])
            ReZp.append(z[3])
            ImZp.append(z[4])
            ReZn.append(z[5])
            ImZn.append(z[6])
        f = np.array(f)
        ReZ = np.array(ReZn)
        ImZ = np.array(ImZn)

        ReZm = np.array(meas.ReZn)
        ImZm = np.array(meas.ImZn)
        fm = np.array(meas.f)
        print((ReZ[-1]) * 1e3, ReZm[-1])
        print((ReZ[-1] - ReZ[0]) * 1e3, ReZm[-1] - ReZm[0])
        print(f[-1], fm[-1])
        dRe = (ReZ - ReZ[0]) * 1e3 - (ReZm - ReZm[0])
        dIm = ImZ * 1e3 - meas.ImZm
        Sum = np.sqrt(sum(dRe ** 2) + sum(dIm ** 2))
        #    print("Sum: ", Sum)
        return Sum
    except:
        return np.nan


outpath = pathlib.Path("../output/var")
outpath.mkdir(parents=True, exist_ok=True)
runVar([0.2, 0.2, 1.6e-06, 1e-06, 1e-06, 1e-4, 3e-3])
quit()
with open("{}/{}.csv".format("../output/var", "opt"), "w") as f:
    _csv = csv.writer(f, delimiter="\t")
    _csv.writerow(["a1", "a2", "S", "k1", "k2", "k3", "k4"])
res = minimize(
    runVar,
    (0.2, 0.5, 1.6e-6, 1e-6, 1e-6, 1e-4, 3e-3),
    bounds=[
        (1e-1, 1),
        (1e-1, 1),
        (1e-7, 1e-3),
        (1e-8, 1e-4),
        (1e-8, 1e-4),
        (1e-8, 1e-2),
        (1e-8, 1e-2),
    ],
    method="TNC",
    options={"disp": True},
)
print(res)
with open("{}/{}.csv".format("../output/var", "opt"), "a") as f:
    _csv = csv.writer(f, delimiter="\t")
    _csv.writerow(["Final", *res])
