import sys

import dolfin
import multiphenics as mp
import petsc4py.PETSc
from petsc4py.PETSc import TS

crmap = {
    0: "TS_CONVERGED_ITERATING",
    1: "TS_CONVERGED_TIME",
    2: "TS_CONVERGED_ITS",
    3: "TS_CONVERGED_USER",
    4: "TS_CONVERGED_EVENT",
    5: "TS_CONVERGED_PSEUDO_FATOL",
    6: "TS_CONVERGED_PSEUDO_FRTOL",
    -1: "TS_DIVERGED_NONLINEAR_SOLVE",
    -2: "TS_DIVERGED_STEP_REJECTED",
}


class PETScTS:
    def __init__(self, statespace, comm):
        self.comm = comm
        self.statespace = statespace

    def createTS(self):
        self.ts = TS().create(self.comm)

    def destroyTS(self):
        self.ts.destroy()

    def setEventHandler(self, handler):
        if handler:
            self.ts.setEventHandler(handler, handler.crossing, handler.terminate)
            if handler.tolerance:
                self.ts.setEventTolerances(tol=handler.tolerance)
            handler.initevents(self.ts, self.t, self.statespace.u.block_vector().vec())
        else:
            self.ts.setEventHandler(None, None, None)

    @property
    def h(self):
        h = self.ts.getTimeStep()
        return h

    @h.setter
    def h(self, _h):
        self.ts.setTimeStep(_h)

    @property
    def t(self):
        t = self.ts.getTime()
        return t

    @t.setter
    def t(self, _t):
        self.ts.setTime(_t)

    @property
    def statespace(self):
        return self._statespace

    @property
    def tmax(self):
        t = self.ts.getMaxTime()
        return t

    @tmax.setter
    def tmax(self, tm):
        self.ts.setMaxTime(tm)

    @statespace.setter
    def statespace(self, x):
        if hasattr(self, "_statespace"):
            raise AttributeError("StateSpace already set")
        self._statespace = x

    def setTolerances(self, atol, rtol):
        self.ts.setTolerances(rtol, atol)

    def setSolution(self, u0):
        x0 = u0.block_vector()
        self.ts.setSolution(x0.vec().copy())

    def integrateToTtarget(self, t_target):
        self.tmax = t_target
        u = self.statespace.u
        x = u.block_vector().vec().copy()
        self.ts.solve(x)  # self._getVector(self.statespace.u).vec())
        cr = self.ts.getConvergedReason()
        self.comm.barrier()
        return crmap[cr]

    def _setState(self, u, x):
        y = dolfin.PETScVector(x)
        y.update_ghost_values()
        u.block_vector().zero()
        u.block_vector().add_local(y.get_local())
        u.block_vector().apply("add")
        u.apply("to subfunctions")

    #    x.ghostUpdate()
    #    u.block_vector().zero()
    #    u.block_vector().add_local(x.getArray())
    #    u.block_vector().apply("add")
    #    u.apply("to subfunctions")

    def restartStep(self):
        self.ts.restartStep()

    def reset(self):
        self.ts.reset()

    def setEvent(self, eventhandler, directions, terminate, *args):
        self._eventhandler = eventhandler
        self.ts.setEvent(self._eventhandler, directions, terminate, *args)

    def _setupTS(self, parameter=None):
        if parameter:
            for (key, value) in parameter.items():
                if key == "type":
                    self.ts.setType(getattr(self.ts.Type, value.upper()))
                if key == "exactfinaltime":
                    self.ts.setExactFinalTime(
                        getattr(self.ts.ExactFinalTime, value.upper())
                    )
                if key == "problemtype":
                    self.ts.setProblemType(getattr(self.ts.ProblemType, value.upper()))
                if key == "equationtype":
                    self.ts.setEquationType(
                        getattr(self.ts.EquationType, value.upper())
                    )
                if key == "arkimex":
                    self._setupARKIMEX(value)
        self.ts.setFromOptions()

    def _setupARKIMEX(self, parameter):
        for (key, value) in parameter.items():
            if key == "type":
                self.ts.setARKIMEXType(getattr(self.ts.ARKIMEXType, value.upper()))
            if key == "fullyimplicit":
                self.ts.setARKIMEXFullyImplicit(value)

    def _setupSNES(self, snes, _type, snesparameter=None):
        snes.setType(_type)
        if snesparameter:
            atol, rtol, stol, maxit = snes.getTolerances()
            snes.setTolerances(
                max_it=snesparameter.get("maxit", maxit),
                atol=snesparameter.get("atol", atol),
                rtol=snesparameter.get("rtol", rtol),
                stol=snesparameter.get("stol", stol),
            )
            for (key, value) in snesparameter.items():
                if key == "forceiteration":
                    snes.setForceIteration(value)
        snes.setFromOptions()

    def _prestep(self, ts):
        return

    def _poststep(self, ts):
        return

    def _prestage(self, ts, t):
        return
