import sys

import dolfin
import multiphenics as mp
import numpy as np
import petsc4py.PETSc
from helper.snesboundchecker import SNESBoundChecker
from timeintegration.petscts import PETScTS

# solving G=M*udot+ F(u,udot) =0
eps = np.finfo(float).eps


class TSMACRO(PETScTS):
    def __init__(self, problem, statespace, comm):
        self.problem = problem
        super().__init__(statespace, comm)
        opts = petsc4py.PETSc.Options()
        opts["snes_linesearch_type"] = "basic"
        opts["snes_divergence_tolerance"] = 1e20
        opts["ts_adapt_dt_max"] = 1e3
        opts["ts_adapt_dt_min"] = 1e-8
        #    opts['ts_adapt_clip'] = '0.01,5'
        #    opts["ts_bdf_order"]= "3"

    def createTS(self, parameter=None):
        super().createTS()
        self._setupTS(parameter.get("ts", None))
        self.ts.setMaxSNESFailures(-1)
        self.ts.setMaxStepRejections(20)
        snes = self.ts.getSNES()
        self._setupSNES(snes, snes.Type.NEWTONLS, parameter.get("snes", None))

    def _setupSNES(self, snes, _type, snesparameter=None):
        super()._setupSNES(snes, _type, snesparameter)
        ksp = snes.getKSP()
        # ksp.setType(ksp.Type.BICG)
        ksp.setType(ksp.Type.PREONLY)
        pc = ksp.getPC()
        pc.setType(pc.Type.LU)
        # pc.setFactorSolverType('klu')
        pc.setFactorSolverType("mumps")

    def _evalG(self, ts, t, x, xdot, f):
        self._setState(self.statespace.u, x)
        self._setState(self.statespace.udot, xdot)
        # dont move out top pre/post step/stage
        self.problem.sett(t)
        self.problem.calculateCoefficients(self.statespace.u)
        # it will cause jumpy solutions
        self._assembleG(self.G, f)

    def _assembleG(self, blockform, petsc_residual=None):
        if petsc_residual is None:
            return mp.block_assemble(blockform)

        else:
            self.r = mp.la.BlockPETScVector(petsc_residual)
            mp.block_assemble(blockform, block_tensor=self.r)

    def _assembleJ(self, blockform, petsc_matrix=None):
        if petsc_matrix is None:
            return mp.block_assemble(blockform, keep_diagonal=True)

        else:
            self.J = mp.la.BlockPETScMatrix(petsc_matrix)
            mp.block_assemble(blockform, block_tensor=self.J, keep_diagonal=True)

    def _evalJacobian(self, ts, t, x, xdot, a, A, B):
        # assert A == B
        if not np.isfinite(a):
            a = 1e12
        self.a.assign(a)
        # self._assembleJ(self.J_Form, B)
        self._assembleJ(self.J_Form, self.J.mat())
        B.zeroEntries()
        B.assemble(B.AssemblyType.FINAL)
        B.axpy(1, self.J.mat())
        B.assemble(B.AssemblyType.FINAL)
        A.zeroEntries()
        A.assemble(A.AssemblyType.FINAL)
        A.axpy(1, self.J.mat())
        A.assemble(A.AssemblyType.FINAL)

        return True

    def createSolver(self, F, bounds=None, parameter=None):
        if hasattr(self, "ts"):
            t0 = self.t
            self.destroyTS()
        else:
            t0 = 0.0

        self.createTS(parameter=parameter)
        self.t = t0

        V = self.statespace.V
        v = mp.BlockTestFunction(V)
        du = mp.BlockTrialFunction(V)
        dudot = mp.BlockTrialFunction(V)
        u = self.statespace.u
        udot = self.statespace.udot
        self.n = len(u)

        def G(u, udot, v, a=None):
            h = F(u=u, udot=udot, v=v)
            if a is None:
                return h
            r = [0] * len(u)
            for i in range(len(v)):
                #        if h[i] is not 0:
                r[i] = a * h[i]
            return r

        self.G = G(u, udot, v)
        self.r = self._assembleG(self.G)

        self.dGdu = mp.block_derivative(G(u, udot, v), u, du)
        self.a = dolfin.Constant(1.0)
        self.dGdudot = mp.block_derivative(G(u, udot, v, self.a), udot, dudot)
        self.J_Form = mp.BlockForm(self.dGdudot + self.dGdu)
        self.J = self._assembleJ(self.J_Form)

        self.ts.setIFunction(self._evalG, self.r.vec())
        amat = self.J.mat().copy()
        self.ts.setIJacobian(self._evalJacobian, amat, amat)
        self.ts.setPostStep(self._poststep)
        self.ts.setPreStep(self._prestep)
        self.ts.setPreStage(self._prestage)
        if bounds:
            snes = self.ts.getSNES()
            lb, ub = bounds
            self.lb = lb
            self.ub = ub
            self._setupSNES(snes, snes.Type.VINEWTONRSLS)
            snes.setVariableBounds(lb.block_vector().vec(), ub.block_vector().vec())

            # self.boundchecker=SNESBoundChecker(self.lb.block_vector())
            # snes.setLineSearchPreCheck(self.boundchecker)

    def _poststep(self, ts):
        t = ts.getTime()
        # self._setState(self.statespace.u, ts.getSolution())
        # self.problem.calculateCoefficients(self.statespace.u)
        self.problem.output(t)
        # self._checkbounds(self.statespace.u.block_vector().vec(), "poststep")

    def _prestep(self, ts):
        return

    def _prestage(self, ts, t):
        return

    def _checkbounds(self, x, where):
        if hasattr(self, "lb"):
            y = dolfin.PETScVector(x)
            lb = self.lb.block_vector().vec().getArray(True)
            bvec = (x.getArray(True) + eps) < lb
            if np.any(bvec):
                print("%s /" % where)
                tmp = x.getArray(True)
                print(sum(bvec))
                print(tmp[bvec])
                print("/%s" % where)
        else:
            pass
